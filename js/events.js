var mainNav = document.getElementsByTagName('nav')[0].children[2];
var congressNav = document.getElementById('congressNav');
var helpIcons;// = document.getElementById('formContactDiv').getElementsByTagName('span');
var newsTiles = document.getElementsByClassName('newsObj');

for (var i = 0; i < mainNav.children.length; i++) {
	mainNav.children[i].addEventListener('click', function(e) {
		e.stopImmediatePropagation();
		if (this.dataset.name.indexOf('m_') === -1 || this.dataset.name.indexOf('m_main') !== -1) {  
			var n = this.dataset.name.split('#');
			if (window.location.toString().indexOf(n[0]) === -1 && window.location.toString().indexOf('m_') !== -1) {
				window.location = "?" + this.dataset.name;
				return;
			}
			if (window.performance)
				scrollToElement(document.getElementById(n[1]), 1500, [mainNav.clientHeight, 
				((n[1] !== 'congress') ? congressNav.clientHeight : congressNav.clientHeight * 2)]);
			else {
				window.location = "#" + n[1];
	// 			window.reqAnimID = true;
				//window.pageYOffset -= 100;
			}
			window.history.replaceState('', '', '#' + n[1]);
		}
		
		else {
			window.location = "?" + this.dataset.name;
		}
		for (var j = 0; j < mainNav.children.length; j++) mainNav.children[j].classList.remove('active');
		this.classList.add('active');
		this.parentNode.style.display= '';
	}
	);
}
if (congressNav)
	for (var i = 0; i < congressNav.children[0].children.length; i++) {
		congressNav.children[0].children[i].addEventListener('click', function(e) {
			e.stopImmediatePropagation();
			if (window.performance)
				scrollToElement(document.getElementById(this.dataset.name), 1500, [mainNav.clientHeight, congressNav.clientHeight * 3]);
			else {
				window.location = "#" + this.dataset.name;
	// 			window.reqAnimID = true;
			}
			window.history.replaceState('', '', '#' + this.dataset.name);
	// 		if (!window.reqAnimID) {
	// 			window.location.href = window.location.href;
	// 		}
	// 		else 
	// 			delete window.reqAnimID;
			for (var j = 0; j < congressNav.children[0].children.length; j++)  congressNav.children[0].children[j].classList.remove('active');
			this.classList.add('active');
		});
	}

for (var i = 0; i < newsTiles.length; i++) {
	newsTiles[i].addEventListener('click', function(e) {
		var source = this.children[0];
		newsDiv.children[0].children[1].innerHTML = '<h3>' + source.getElementsByTagName('h3')[0].innerHTML + '</h3>' + source.getElementsByTagName('div')[1].innerHTML;
		if (source.dataset.gallery) {
			var json = new Function('return ' + source.dataset.gallery)();
			//console.log(json);
			//console.log(source);
			for (z = 0; z < json.length; z++) {
				var img = document.createElement('img');
				img.src = json[z].mini;
				img.className = 'newsImg';
				console.log(img);
				newsDiv.children[0].children[1].appendChild(img);
			}
		}
		//for (var z = 0; z < source.dataset.gallery.length
		//newsDiv.children[0].children[1].innerHTML = this.children[0].innerHTML;
		newsDiv.style.display = 'block';
		window.setTimeout(function() {newsDiv.children[0].style.transform = "scale3d(1.0,1.0,1)";}, 100);
	});
}

upClickBtn.addEventListener('click', function(e) {
	if (window.performance)
		scrollVerticalAnimation(document.scrollingElement || document.body, 1500);
	else {
		window.location = '#';
// 		window.reqAnimID = true;
	}
	window.history.replaceState('', '', '#');		
// 	if (!window.reqAnimID) {
// 			window.location.href = window.location.href;
// 		}
// 		else 
// 			delete window.reqAnimID;
// 	window.location.reload();
});

document.getElementsByTagName('nav')[0].children[0].addEventListener('click', function(e) {
	if (window.performance)
		scrollVerticalAnimation(document.scrollingElement || document.body, 1500);
	else {
		//poprawić!!
		window.location = '?m_main#mainContener';
// 		window.reqAnimID = true;
	}
	if (window.location.toString().indexOf('m_') === -1 || window.location.toString().indexOf('m_main') !== -1) 
		window.history.replaceState('', '', '#');
	else
		window.location = "?m_main#mainContener";
// 	if (!window.reqAnimID) {
// 			window.location.href = window.location.href;
// 			
// 	}
// 	else 
// 		delete window.reqAnimID;
// 	window.location.reload();
});

mainNav.parentNode.children[1].addEventListener('click', function(e) {
	mainNav.style.display = 'block';
});

if (congressNav) {
	document.getElementById('plFlag').addEventListener('click', changeLang);
	document.getElementById('enFlag').addEventListener('click', changeLang);
}

document.body.addEventListener('mousewheel', moveMouseWheel);

window.addEventListener('scroll', moveMouseWheel);

newsDiv.addEventListener('click', closeNews);
newsDiv.children[0].addEventListener('click', function(e) {e.stopPropagation();});

document.getElementById('exit').addEventListener('click',closeNews);