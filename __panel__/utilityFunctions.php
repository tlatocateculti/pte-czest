<?php

function confirmSession() {
	global $_BASE_LOCATION;
	@session_start();
	if (!isset($_SESSION['login'])) {
		if (!isset($_SESSION['loginTry']))
			$_SESSION['loginTry'] = 3;
		else 
			$_SESSION['loginTry']--;
		if ($_SESSION['loginTry'] == 0 && !isset($_SESSION['login'])) {
			unset($_SESSION['unconfirm']);
			unset($_SESSION['loginTry']);
			unset($_SESSION['sid']);
			header('Location: ' . $_BASE_LOCATION[0]);
		}
	}
	if ($_SERVER['QUERY_STRING'] == "showLogout") {
		$_SESSION['unconfirm'] = false;
		unset($_SESSION['login']);
		header('Location: ' . $_BASE_LOCATION[0]);
		//header('Location: ' . $_BASE_LOCATION[0]);
	}
	if ((isset($_POST['pass']) && isset($_POST['login'])) || isset($_SESSION['sid'])) {
		require_once(__DIR__ . "/../scripts/utilityFunctions.php");
		if (isset($_SESSION['sid'])) {
			$ses = explode('&&', $_SESSION['sid']);
		}
		if ($f = @fopen(__DIR__ . '/../data/login.dat', 'r')) { 
			$sRead = fread($f, filesize("../data/login.dat"));
			fclose($f);
			//jak to jest liczone trzeba sobie z kodu wywnioskować (na razie)
			$sRead = explode('|##|', $sRead);
			//print_r($sRead);
			for ($i = 0; $i < count($sRead); $i++) {
				$data = explode('||', decodePhrase($sRead[$i]));
				if (isset($ses)) {
					if($ses[1] === $data[2] && $ses[2] == $i) {
						$_SESSION['unconfirm'] = true;
						unset($_SESSION['loginTry']);
						$_SESSION['login'] = 'mail';
						$_SESSION['mail'] = $data[2];
						break;
					}
					else {
						$_SESSION['unconfirm'] = false;
						unset($_SESSION['sid']);
					}
				}
				else {
					$l = md5($_POST['login'] . $data[2] . $_POST['pass']);
					$p = md5($_POST['login'] . $_POST['pass']);
					if ($p != $data[0] || $l != $data[1]) 
						$_SESSION['unconfirm'] = false;
					else {
						$_SESSION['unconfirm'] = true;
						unset($_SESSION['loginTry']);
						$_SESSION['login'] = $_POST['login'];
						$_SESSION['mail'] = $data[2];
						break;
					}
				}
			}
		}
		else 
			$_SESSION['unconfirm'] = false;
		unset($_SESSION['sid']);
	}
	//TUTAJ TRZEBA TO WSZYSTKO POPRAWIĆ - ZA DUŻO WARUNKÓW I SIĘ POGUBIĘ DNIA PEWNEGO!
	if (!isset($_SESSION['unconfirm']))
		header('Location: ' . $_BASE_LOCATION[0]);
	if (!$_SESSION['unconfirm']) {
		//header('Location: ' . $_BASE_LOCATION[0]);
		$_SESSION['unconfirm'] = false;
	}
}

// function imScale($res, $w, $h) {
// 	$sizeW = imagesx($res);
// 	$sizeH = imagesy($res);
// 	$ratio = $sizeW/$sizeH;
// 	$wPrev = $w;
// 	$hPrev = $h;
// 	$xStart = 0;
// 	$yStart = 0;
// 	if ($ratio > 1) { 
// 		$h = $w / $ratio;
// 		if ($h != $hPrev)
// 			$yStart = ($hPrev - $h) / 2;
// 	}
// 	else if ($ratio < 1) { 
// 		$w = $h * $ratio;
// 		if ($w != $wPrev) 
// 			$xStart = ($wPrev - $w) / 2;
// 	}
// 	$im = imagecreatetruecolor($wPrev, $hPrev);
// 	$opacity = imagecolorallocatealpha($im, 0, 0, 0, 127);
// 	imagefill($im, 0, 0, $opacity);
// 	imagecopyresampled($im, $res, $xStart, $yStart, 0, 0, $w, $h, $sizeW, $sizeH);
// 	return $im;
// }
// 
// function generateMiniImages($img, &$miniImg, $path) {	
// 	for ($i = 0; $i < count($img); $i++) {
// 		$hasMini = false;
// 		for ($j = 0; $j < count($miniImg); $j++) {
// 			if (strpos($miniImg[$j], $img[$i]) !== false) {
// 				$hasMini = true;
// 				break;
// 			}			
// 		}
// 		if (!$hasMini) {
// 			//$hasAlpha = false;
// 			$imgnameArray = explode('.', $img[$i]);
// 			
// 			if ($imgnameArray[1] == 'png') {				
// 				$im = @imagecreatefrompng("{$path}/{$img[$i]}");
// 				//$hasAlpha = true;
// 			}
// 			else {
// 				$im = @imagecreatefromjpeg("{$path}/{$img[$i]}");
// 			}
// 			$im2 = imScale($im, 300, 200);
// // 			$im2 = @imagescale($im, 300, 200);
// // 			if ($hasAlpha) {
// 				@imageAlphaBlending($im2, true);
// 				@imageSaveAlpha($im2, true);
// // 			}
// 			
// 			@imagepng($im2, "{$path}/{$imgnameArray[0]}_mini.png");
// 			$miniImg[] = "{$imgnameArray[0]}_mini.png";
// 			@imagedestroy($im2);
// 			@imagedestroy($im);
// 		}
// 	}
// }

//funkcja ma za zadanie usuwać zbędne pliki z podanej ścieżki (folderu). 
//$path - ścieżka (folder), z którego mają zostać usunięte niepotrzebne pliki
//$existed_files - lista plików do usunięcia/pozostawienia. Powinna zawierać jedynie nazwy plików (nie ścieżki!)
//$deleteSelected - domyślnie false; jeżeli false to wskazane wyżej pliki mają POZOSTAĆ w katalogu; jeżeli true to wskazane pliki mają zostać SKASOWANE
function removeGalleryFiles($path, $existed_files = '', $deleteSelected = false) {
//'../galleries/' . $bs->getResults(0)['gallery_folder'][$i]
	if (!$deleteSelected) {
		$folder_files = @scandir($path);
		for ($z = 0; $z < count($folder_files); $z++) {
			if ($folder_files[$z] == '..' || $folder_files[$z] == '.') {
				$folder_files[$z] = '';
				continue;
			}
			//$base_files
			for ($y = 0; $y < count($existed_files); $y++) {
				if ($folder_files[$z] == $existed_files[$y]) {
					$folder_files[$z] = '';
					break;
				}
			}
			if ($folder_files[$z] != '')
				unlink($path . '/' . $folder_files[$z]);
		}
	}
	else {
		for ($i = 0; $i < count($existed_files); $i++) {
			unlink($path . '/' . $existed_files[$i]);
		}
	}
}
