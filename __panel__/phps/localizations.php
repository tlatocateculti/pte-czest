<?php
	$noResults = false;
	//pliki wymagane, jednak nie ma sensu podłaczać ich wielokrotnie
	//require_once "../scripts/database.php";
	//require_once "Mail.php";
	require_once('./utilityFunctions.php');
	require_once "../../config.php";
	require_once "../../scripts/utilityFunctions.php";
	
 	if (!isset($_SESSION['limit']['localization']))
		$_SESSION['limit']['localization'] = 0;
		
		
	function createActionButtons($hide = 0, $n = [], $icons = [], $actions = []) {
		$btn = createNodeButtons($hide, $n, $icons, [], $actions);
		$btnTxt = '';
		for ($i = 0; $i < count($btn); $i++) {
			$btnTxt .= "<div style=\"position: relative;\"";
			if (isset($btn[$i]['style']))
				$btnTxt .= " class=\"{$btn[$i]['style']}\"";
			if (is_array($btn[$i]['events'])) {
				$keys = array_keys($btn[$i]['events']);
				for ($j = 0; $j < count($keys); $j++) {
					$btnTxt .= " {$keys[$j]}='" . $btn[$i]['events'][$keys[$j]] . ";'";
				}
			}
			$btnTxt .= ">";
			if (isset($btn[$i]['icon'])) {
				$btnTxt .= "<p";
				if ($btn[$i]['name'])
					$btnTxt .= " data-tip='{$btn[$i]['name']}'";
				$btnTxt .= ">{$btn[$i]['icon']}</p>";
			}
			else 
				$btnTxt .= "<p>{$btn[$i]['name']}</p>";
			$btnTxt .= "</div>";
		}
		return $btnTxt;
	}
	
	
	function createLocalizationPanel() {
		echo '<div class="settingsSetDiv">
			<div class="headerSetName" style="cursor: pointer; color: white; font-weight: 900;" onclick="localizationAddEdit(false, this);">
				<div style="float: left;"><p>Dodaj nową lokalizację</p></div>
				<div style="float: right; width: 170px; height: 100%; overflow:hidden; font-size: 15px;">
				</div>
			</div>
		</div>';
		$btnIcons = [0 => '<i class="fa fa-cog" aria-hidden="true"></i>', 2 => '<i class="fa fa-trash" aria-hidden="true"></i>'];
		$btnName = [0 => 'Edytuj lokalizację', 2 => 'Usuń lokalizację (NA ZAWSZE)'];
		$btnActions = ['onclick' => array("event.stopPropagation(); sitesTableAddEdit(true, false, this)", 
						"event.stopPropagation(); showHidesites(false, this)", 
						"event.stopPropagation(); sitesTableDelete(false, this)") ];
		$file = readEntries('../../sites', 'contact.json');
		for ($i = 0; $i < count($file); $i++) {
			if ($file[$i]['deleted'] == 0) {
				$btnIcons[1] = '<i class="fa fa-eye-slash" aria-hidden="true"></i>';
				$btnName[1] = 'Ukryj lokalizację';
			}
			else {
				$btnIcons[1] = '<i class="fa fa-eye" aria-hidden="true"></i>';
				$btnName[1] = 'Pokaż lokalizację';
			}
			$address = explode(';;', $file[$i]['address']);
			echo '<div class="settingsSetDiv">
				<div class="settingsBody" data-addressname="' . $file[$i]['addressname'] . '" data-comment="' . $file[$i]['comment'] . '" data-streetprefix="' . $address[0]  . '" data-street="' .$address[1] . '" data-placenum="' . $address[2] . '" data-localnum="' . $address[3] . '" data-postalcode="' . $file[$i]['postalcode'] . '" data-postoffice="' . $file[$i]['postoffice'] . '" data-town="' . $file[$i]['town'] . '" data-tels="' . $file[$i]['telephones'] . '" data-opens="' . $file[$i]['workHours'] . '" data-lat="' . $file[$i]['lat'] . '" data-lon="' . $file[$i]['lon'] . '" data-zoom="' . $file[$i]['zoom'] . '" data-deleted="' . $file[$i]['deleted'] . '" data-id="' . $i . '" onclick="sitesTableAddEdit(true, false, this);">
					<div style="float: left;"><p>' . ($file[$i]['description'] !== '' ? $file[$i]['description'] : '{brak opisu}')  . ($file[$i]['deleted'] == 0 ? ' (aktywna)' : ' (nieaktwyna)') . '</p></div>
					<div class="buttonIconDiv">' . 
					createActionButtons($file[$i]['deleted'], $btnName, $btnIcons, $btnActions) .'</div>
					<div style="display: none;">' . $file[$i]['text'] . '</div>
				</div>
			</div>';
		}
	}
?>
<style>

	form div {
		margin-bottom: 15px;
		margin-left: 20%;
	}
	form p {
		font-size: 20px;
		font-weight: bold;
		text-align: center;
	}
</style>

<style>
.shortInput {
	width: 60px;
}
.deleted {
	background: black;
	color: white;
}
#localizationForm {
/* 	display: none; */
}
#localizationWindows, #telephonesWindow {
	display: none;
	position: absolute;
	background: white;
	border: 1px solid black;
}
#localizationHoursList, #telephonesList {
	overflow: hidden;
}
.afterDiv:after {
	content: '';
	display: table;
	clear: both;
}
.ol-zoom {
	display: none;
}
}
</style>
<link rel="stylesheet" href="./css/elements.css"/>
<div id="localizationContent"></div>
<div id="tipLabel"><p></p></div>
<div id='localizationWindows'>
	<div><input type='checkbox' value='0'>Poniedziałek</div>
	<div><input type='checkbox' value='1'>Wtorek</div>
	<div><input type='checkbox' value='2'>Środa</div>
	<div><input type='checkbox' value='3'>Czwartek</div>
	<div><input type='checkbox' value='4'>Piątek</div>
	<div><input type='checkbox' value='5'>Sobota</div>
	<div><input type='checkbox' value='6'>Niedziela</div>
	<div id='hoursRange'></div>
	<div><span>Przedział czasowy:</span>od<select name='hourStart'>
	<?php 
		for ($i = 0; $i < 24; $i++)
			echo "<option value='{$i}'>{$i}</option>";
	?>
	</select>
	<select name='minStart'>
	<?php 
		for ($i = 0; $i < 60; $i++)
			echo "<option value='{$i}'>{$i}</option>";
	?>
	</select>do
	<select name='hourStop'>
	<?php 
		for ($i = 0; $i < 24; $i++)
			echo "<option value='{$i}'>{$i}</option>";
	?>
	</select>
	<select name='minStop'>
	<?php 
		for ($i = 0; $i < 60; $i++)
			echo "<option value='{$i}'>{$i}</option>";
	?>
	</select><button onclick='addHourRange();'>Dodaj przedział</button></div>
	<button onclick='addNewReception(this);'>Dodaj zakres otwarcia</button>
	<button onclick='closeWindow(this);'>Anuluj</button>
</div>
<div id='telephonesWindow'>
	<div><span>Rodzaj kontaktu:</span><select name='telType' onchange="generateTelForm(this);">
	<option value='0'>Telefon komórkowy</option>
	<option value='1'>Telefon stacjonarny</option>
	<option value='3'>Fax</option>
	<option value='4'>Tel/fax</option>
	<option value='5'>Email</option>
	<option value='2'>Telefon/komunikator Internetowy</option>
	</select></div>
	<div id='telephoneForm'></div>
<!--	<div><span>Kierunkowy kraju</span><input type='text' name='prefix' placeholder='Format: (+xx)' value='+48'/></div>
	<div><span>Numer telefonu</span><input type='text' name='telephone' placeholder='Format: xxx-xxx-xxx'/></div>-->
	<button onclick='addNewTelephone(this);'>Dodaj telefon</button>
	<button onclick='closeWindow(this);'>Anuluj</button>
</div>
