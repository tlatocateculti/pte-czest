<?php 
	require_once('./utilityFunctions.php');
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
// 	require_once "../mailFunctions.php";
	require_once "../../scripts/utilityFunctions.php";
	require_once "../scripts/forms.php";
	confirmSession();
	
	function createLocalizationForm($data = '') {
		$date = new DateTime("now");
		$date->setTimezone(new DateTimezone("Europe/Warsaw"));	
		echo "<div id='localizationForm' style='overflow: hidden;'>";
		//createTextInput("addDate", ($data === '' ? $date->format("Y/m/d H:i") : $data['addDate'][0]), 'Data dodania', 'Format: YYYY/MM/DD HH:MM', 'afterBlock beforeBlock');
		echo createNewTextInput("addressname", ($data === '' ? '' : $data['addressname']), 'Nazwa oddziału', 'Pełna nazwa lokalizacji', 'afterBlock beforeBlock', 'Należy podać nazwę naszej lokalizacji. W przypadku np. biura firmy najlepiej jest podać pełną nazwę firmy.');
		echo createNewTextInput("comment", ($data === '' ? '' : $data['comment']), 'Komentarz do oddziału (niewidoczny na stronie)', 'Opis oddziału, jedynie do naszej wiadomości (opcjonalny)', 'afterBlock beforeBlock', 'Komentarz widoczny jest jedynie w tym miejscu. W przypadku wielu adresów pozwala na zachowanie dodatkowych informacji.');
		echo createNewSelectInput("streetprefix", array(0 => 'ul.', 1 => 'al.', 2 => 'pl.'), ($data === '' ? '' : $data['streetprefix']), 'Prefiks ulicy', '', 'afterBlock beforeBlock', 'Prawidłowy przedrostek naszego adresu. ul. - ulica, al. - aleja, pl. - plac');
		echo createNewTextInput("street", ($data === '' ? '' : $data['street']), 'Ulica', 'Nazwa ulicy, bez przedrostka bądź numerów budynku/lokalu', 'afterBlock beforeBlock', 'Pełna bądź skrócona nazwa ulicy naszej lokalizacji;  pole to może zostać zaproponowane przez mapę (np. zostanie poprawiona nazwa)');
		echo createNewTextInput("placenum", ($data === '' ? '' : $data['placenum']), 'Numer posesji', 'Numer budynku i/lub posesji', 'afterBlock beforeBlock', 'Numer działki/budynku naszej lokalizacji. Można podawać numery łamane i/lub z literami (np. 23/5a)');
		echo createNewTextInput("localnum", ($data === '' ? '' : $data['localnum']), 'Numer lokalu', 'Opcjonalny numer lokalu', 'afterBlock beforeBlock', 'Numer lokalu przez nas zajmowanego. Można podawać numery łamane i/lub z literami (np. 23/5a)');
		echo createNewTextInput("town", ($data === '' ? '' : $data['town']), 'Miejscowość', 'Nazwa miejscowości/miasta', 'afterBlock beforeBlock', 'Pełna bądź skrócona nazwa miejscowości. Wartość ta może zostać zaproponowana przez mapę (np. po kodzie pocztowym)');
		echo createNewTextInput("postalcode", ($data === '' ? '' : $data['postalcode']), 'Kod miejscowości', 'Kod pocztowy miejscowości', 'afterBlock beforeBlock', 'Kod miejscowości naszej lokalizacji. Wartość ta może zostać zaproponowana przez mapę (np. po nazwie miejscowości)');
		echo createNewTextInput("postoffice", ($data === '' ? '' : $data['postoffice']), 'Poczta', 'Nazwa miejscowości z placówką pocztową', 'afterBlock beforeBlock', 'Jeżeli poczta znajduje się w innej miejscowości to tutaj można podać nazwę tej miejscowości. Wartość ta jest przeważnie uzupełniana przez mapę (na podstawie kodu pcoztowego)');
		echo createNewTextInput("country", ($data === '' ? '' : $data['country']), 'Państwo', 'Kraj naszej lokalizacji', 'afterBlock beforeBlock', 'Nazwa kraju, w którym posiadamy lokalizację. Przeważnie jest ona proponowana przez mapę (na podstawie nazwy miejscowości i/lub kodu pocztowego)');
		//echo createNewTextInput("postalcode", ($data === '' ? '' : $data['postalcode']), 'Komentarz strony', 'Wprowadzony tekst będzie widoczny jedynie w panelu administracyjnym!', 'afterBlock beforeBlock', 'Opis strony. Nie jest on nigdzie wyświetlany (poza panelem administracyjnym) i ułatwia on jedynie rozpoznawanie kolejnych podstron (to on wyświetla się w menu wyboru stron).');
// 		echo createNewTextInput("tels", ($data === '' ? '' : $data['tels']), 'Kontakt', 'Wprowadzony tekst będzie widoczny jedynie w panelu administracyjnym!', 'afterBlock beforeBlock', 'Opis strony. Nie jest on nigdzie wyświetlany (poza panelem administracyjnym) i ułatwia on jedynie rozpoznawanie kolejnych podstron (to on wyświetla się w menu wyboru stron).');
// 		echo createNewTextInput("opens", ($data === '' ? '' : $data['opens']), 'Godziny otwarcia', 'Wprowadzony tekst będzie widoczny jedynie w panelu administracyjnym!', 'afterBlock beforeBlock', 'Opis strony. Nie jest on nigdzie wyświetlany (poza panelem administracyjnym) i ułatwia on jedynie rozpoznawanie kolejnych podstron (to on wyświetla się w menu wyboru stron).');
		echo "<div class='afterDiv'><span>Telefony</span><div id='telephonesList'></div><button onclick='openWindow(0);'>Dodaj opcję kontaktu</button>" . createNewInfoButton('Przycisk otwiera nowe okno, w którym możemy dodawać wszelkie formy kontaktu z nami. Przykładowo możemy dodać telefon (stacjonarny bądź komórkowy), identyfikator komunikatora interetowego bądź adres poczty elektornicznej') . "</div>
	<div><span>Dni/godziny pracy</span><div id='localizationHoursList'></div><button onclick='openWindow(1);'>Dodaj nowe</button>" . createNewInfoButton('Przycisk otwiera okno, w którym możemy dodawać godziny otwarcia naszego oddziału. W oknie zaznaczamy dni, do których ma odnosić się przedział, a nasttępnie godziny otwarcia (od do). Możemy dodać więcej niż jeden przedział przedział do wskazanych dni. Możemy kilkukrotnie dodawać godziny otwarcia (dla różnych dni)') . "</div>";
		$zoomList;
		for($i = 0; $i < 20; $i++) $zoomList[$i] = $i;
		echo createNewSelectInput("mapzoom", $zoomList, ($data === '' ? '' : $data['zoom']), 'Powiększenie mapy', ['onchange' => "document.getElementsByName('zoom')[0].value = this.value;"], 'afterBlock beforeBlock', 'Ustawia optymalne przybliżenie mapy na naszą lokalizację.');
		echo createNewTextInput("maplink", '', 'Odnośnik do mapy', 'Użyć w przypadku, gdy lokalizacja na mapie jest błędnie wskazana lub mamy poprawny odnośnik do naszej lokalizacji!', 'afterBlock beforeBlock', 'W pole to można wkleić adres WWW do mapy naszej lokalizacji. Większość pól naszego formularza zostanie samoczynnie uzupełniona. Jeżeli pole to zawiera odnośnik do mapy to pozostałe pola nie będą zmieniały swojej zawartości (możemy je ręcznie modyfikować). Odnośniki do map mogą pochodzi z następujących portali: https://google.com/maps, https://wego.here.com/ lub http://www.openstreetmap.org');
		createHiddenInput("user_id", 1);
		createHiddenInput("idEdit", ($data === '' ? '-1' : $data['fields']));
		createHiddenInput("lat", ($data === '' ? 0 : $data['lat']));
		createHiddenInput("lon", ($data === '' ? 0 : $data['lon']));
		createHiddenInput("zoom", ($data === '' ? 0 : $data['zoom']));
		echo '
		<p>Wizualizacja mapy lokalizacji</p>
			<div id="contenermap">
				<div id="map" >
					<div style="overflow: hidden;height:300px;width:100%;">
						<div id="gmap_canvas" style="height:100%;width:100%;"></div>
					</div>
				</div>
			</div>';
		echo "</div>";
	}
	
	function indexOf($table, $search) {
		$maxCount = count($table);
		$max = (((int) ($maxCount--/2)) + 1);
		for ($i = 0; $i < $max; $i++) {
			if ($table[$i] == $search)
				return $i;
			if ($table[$maxCount - $i] == $search)
				return $maxCount - $i;
		}
		return -1;
	}
	
	$i = 0;
	
	if (isset($_POST['fields'])) {
		if ($_POST['fields'] == '') {
			echo '-100';
			return;
			//header('Location: sites_maintenance.php?clear=1');
		}

		//$files = readEntries('../../sites', 'contact.json', '', $_POST['fields']);
		//$files['idEdit'] = $_POST['fields'];
		if (count($_POST) > 0)
			createLocalizationForm($_POST);
		else
			createLocalizationForm();
	}
	else {
		createLocalizationForm();
	}
?>

<style>
	.afterBlock:after, .beforeBlock:before {
		content: '';
		overflow: auto;
		display: table;
		clear: both;
	}
	.selectList {
		overflow: hidden;
	}
	input {
		width: 50%;
	}
/*	.expired {
		color: red;
	}*/
/*	#galleryWindow {
        display: none;
        position: absolute;
        background: white;
        border: 1px solid black;
    }
    #galleriesDiv {
        overflow-y: auto;
        height: 90%;
       /* position: relative;*/
    }*/
    
    
    	table {
		position: relative;
		width: 100%;
		overflow: hidden;
		table-layout: fixed;
	}
	th,td {
		width: 100px;
		overflow: hidden;
		word-break: break-all;
	}
	th:first-child {
		width: 30px;
	}
	th:last-child {
		width: 60px;
	}
	#content {
		overflow-x: visible;
	}
	.gallerySetDiv {
		border: 1px solid black;
		overflow: hidden;
		width: 95%;
		height: 40px;
	}
	.gallerySetName {
		font-size: 20px;
		font-weight: bold;
		text-align: center;
		background: gray;
		margin: 0;
		padding-top: 10px;
		height: 40px;
		cursor: pointer;
	}
	
	.photosSet {
		overflow: hidden;
		overflow-y: auto;
		height: 420px;
	}
	
	.photosSet:after {
		content: '';
		clear: both;
		display: table;
	}
	
	.photoMiniSet {
		width: 300px;
		height: 200px;
		float: left;

	}
	.actionButtons {
        display: none;
	}
</style>

<button onclick="saveLocalization();">
<?php if (isset($_POST['fields'])) echo 'Zapisz zmiany'; else {
		echo 'Dodaj lokalizację'; 
	}?></button>
<button onclick="getLocalizations();">Anuluj</button>