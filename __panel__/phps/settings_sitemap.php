<?php
	require_once('./utilityFunctions.php');
	require_once "../../scripts/utilityFunctions.php";
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
/**
 * Klasa dziedziczaca po XMLWriter; Jej zadaniem jest 
 * tworzyc aktualna mape serwisu WWW tak, aby ulatwic jego
 * indeksowanie przez wyszukiwarki sieciowe.
 * @author Tezcatlipoca
 * @version 1.0
 *
 */
class SiteMap extends XMLWriter
{
	/**
	 * Zmienna przechowuje adres do naszego serwisu, np. 'http://www.akademiaorange.pl'
	 * @var string
	 */
	private $url = '';
	/**
	 * Zmienna będzie przechowywać uchwyt do bazy danych - celem wywoływania poleceń
	 * @var string
	 */
// 	private $bs = '';
	//private $log;
	/**
	 * Konstruktor klasy
	 * @param string $url adres serwisu (domena), dla ktorego ma powstac mapa
	 */
	function __construct($url = "")
	{
		$this->url = $url;
// 		$this->bs = new Database();
// 		$this->bs->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);
	}
	/**
	 * Destruktor klasy
	 */
	function __destruct()
	{
		unset($this->url);
		unset($this->prefix);
	}
	/**
	 * Funkcja tworzy mape strony WWW
	 */
	public function createSiteMap()
	{
		$this->createHeader();
// 		$this->addCoursesTrainings();
// 		$this->addNews();
		$this->addSites();
		$this->endSitemap();
	}
	/**
	 * Funkcja tworzy naglowek mapy strony zawierajacy adres strony, ostatnia modyfikacje, z jaka czestotliwoscia sie zmienia oraz jaki ma priorytet waznosci (wzgledem innym podstron)
	 */
	private function createHeader()
	{
		$this->openMemory();
		$this->startDocument('1.0', 'UTF-8');	
		$this->setIndent(true);
		$this->startElement('urlset');
		$this->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
		
		$this->startElement("url");
		//----------------------------------------------------
		$date = date("Y-m-d");
		$this->writeElement('loc', $this->url);
		$this->writeElement('lastmod', $date);
		$this->writeElement('changefreq', 'yearly');
		$this->writeElement('priority', '0.2');
		//----------------------------------------------------
		$this->endElement();
		//return true;
	}
	/**
	 * Funkcja dodaje strony projektu do mapy witryny; priorytet 1.0
	 */
// 	private function addCoursesTrainings()
// 	{
// 		$this->bs->flushConditions();
// 		$this->bs->flushResults();
// 		$this->bs->buildConditionQuery(array('courses', 'active'), array(1), DataEnum::EQUAL);
// 		$this->bs->queryTable(array('courses' => array('course_id', 'subcat_id')), $this->bs->getConditions(), -1);
// 		$courses = $this->bs->getResults(0);
// 		$this->bs->flushConditions();
// 		$this->bs->flushResults();
// 		$this->bs->buildConditionQuery(array('subcategories', 'subcat_id'), $courses['subcat_id'], DataEnum::IN);
// 		$this->bs->queryTable(array('subcategories' => array('category_id')), $this->bs->getConditions(), -1);
// 		$this->bs->flushConditions();
// 		$this->bs->buildConditionQuery(array('categories', 'category_id'), $this->bs->getResults(0)['category_id'], DataEnum::IN);
// 		$this->bs->queryTable(array('categories' => array('category_type')), $this->bs->getConditions(), -1);
// 		$courses['category_type'] = $this->bs->getResults(1)['category_type'];
// 		$this->bs->flushConditions();
// 		$this->bs->flushResults();
// 		$date = date("Y-m-d");
// 		for ($i = 0;  $i < count($courses['course_id']); $i++)
// 		{			
// 			$this->startElement("url");
// 			//----------------------------------------------------
// 			$this->writeElement('loc', $this->url. '/?c_' . $courses['subcat_id'][$i] . '/ns_' . $courses['course_id'][$i] . '/m_' . (($courses['category_type'][$i] == 0) ? 'courses' : 'trainings'));
// 			$this->writeElement('lastmod', $date);
// 			$this->writeElement('changefreq', 'weekly');
// 			$this->writeElement('priority', '1.0');
// 			//----------------------------------------------------
// 			$this->endElement();
// 		}
// 	}
// 	/**
// 	 * Funkcja dodaje aktualnosci do mapy witryny; priorytet 0.9
// 	 */
// 	private function addNews()
// 	{
// 		$this->bs->flushConditions();
// 		$this->bs->flushResults();
// 		$this->bs->buildConditionQuery(array('news', 'show'), array(1), DataEnum::EQUAL);
// 		$this->bs->queryTable(array('news' => array('news_id', 'dateAdd')), $this->bs->getConditions(), -1);
// 		for ($i = 0;  $i < count($this->bs->getResults(0)['news_id']); $i++)
// 		{
// 			$this->startElement("url");
// 			//----------------------------------------------------
// 			$this->writeElement('loc', $this->url. '/?a_' . $this->bs->getResults(0)['news_id'][$i]);
// 			$this->writeElement('lastmod', (string)date("Y-m-d", $this->bs->getResults(0)['dateAdd'][$i]));
// 			$this->writeElement('changefreq', 'daily');
// 			$this->writeElement('priority', '0.9');
// 			//----------------------------------------------------
// 			$this->endElement();
// 		}
// 		$this->bs->flushConditions();
// 		$this->bs->flushResults();
// 	}
	/**
	 * Funkcja dodaje strony do mapy witryny; priorytet 0.8
	 */
	private function addSites()
	{
		$date = date("Y-m-d");
		$sites = ['m_index', 'm_offer', 'm_contact'];
		for ($i = 0; $i < count($sites); $i++) {
			$this->startElement("url");
			$this->writeElement('loc', $this->url. '/?' . $sites[$i]);
			$this->writeElement('lastmod', $date);
			$this->writeElement('changefreq', 'yearly');
			$this->writeElement('priority', '0.5');
			$this->endElement();
		}
	}
	/**
	 * Funkjca konczy plik mapy witryny po czym, jezeli to mozliwe, zapisuje go pod wskazany adres
	 */
	private function endSitemap()
	{
		// End urlset
		$this->endElement();
		$this->endDocument();
		//header('Content-type: text/xml; charset=utf-8');
		//header("Content-Disposition: attachment; filename='".$this->file."'");
		$data = $this->outputMemory();
		file_put_contents(__DIR__ . "/../../sitemap.xml",$data);
		//if (!file_exists(__DIR__ . "/../../robots.txt"))
		file_put_contents(__DIR__ . "/../../robots.txt", 'Sitemap: ' . $this->url . '/sitemap.xml'); 
		//if (file_put_contents("./sitemap.xml",$data) === false) return $this->log->printError(106);
		$this->flush();
	}
}
?>