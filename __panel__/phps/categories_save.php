<?php 
	require_once('./utilityFunctions.php');
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
	require_once "../../scripts/utilityFunctions.php";
	require_once "../../scripts/search.php";
	confirmSession();
	
	function getCatPos(&$cats, &$fileKey, &$btnPos = '', &$catName = '') {
		if ($catName === '') {
			$catsName = array_keys($cats);
			for ($k = 0; $k < count($catsName); $k++) {
				$subCats = array_keys($cats[$catsName[$k]]);
				for ($i = 0; $i < count($subCats); $i++) {
					$btnPos = (isset($cats[$catsName[$k]][$subCats[$i]]['buttons']) ? @count($cats[$catsName[$k]][$subCats[$i]]['buttons']) : 0);
					for ($j = 0; $j < $btnPos; $j++) {
						if ($cats[$catsName[$k]][$subCats[$i]]['buttons'][$j]['site'] === $fileKey) {
							$btnPos = $j;
							$catName = $catsName[$i];
							return;
						}
					}
				}
			}
		}
		else {
			$btnPos = (isset($cats[$catName]['buttons']) ? @count($cats[$catName]['buttons']) : 0);
			for ($i = 0; $i < $btnPos; $i++) {
				if ($cats[$catName]['buttons'][$i]['site'] === $fileKey) {
					$btnPos = $i;
					return;
				}
			}
		}
	}

	if (isset($_POST['deleteNode'])) {
		$cats = readEntries('../../sites', 'categories.json');
		$catPos = '';
		$subCount = 0;
		//if (isset($_POST['sub'])) {
		
		if (!isset($_POST['cat']) && !isset($_POST['sub'])) {
			$subCount = count(array_keys($cats[$_POST['id']]));
		}
		else if (isset($_POST['cat']) && !isset($_POST['sub'])) {
			$subCount = count($cats[$_POST['cat']][$_POST['id']]['buttons']);
		}
		
		if ($subCount == 0) {
			if (!isset($_POST['cat']) && !isset($_POST['sub']))
				unset($cats[$_POST['id']]);
			else if (isset($_POST['cat']) && !isset($_POST['sub']))
				unset($cats[$_POST['cat']][$_POST['id']]);
			else {
				unset($cats[$_POST['cat']][$_POST['sub']]['buttons'][$_POST['id']]);
				$cats[$_POST['cat']][$_POST['sub']]['buttons'] = array_values($cats[$_POST['cat']][$_POST['sub']]['buttons']);
			}
			echo 'Skasowano wybraną kategorię/przycisk!';
		}
		else echo 'Nie można skasować - kategoria/podkategoria zawiera elementy od niej zależne. Najpierw trzeba je skasować!';
		//}
// 		uset($cats[$catPos]['buttons'][$btnPos]);
// 		if (count($cats[$catPos]['buttons'])) unset($cats[$catPos]);
		file_put_contents('../../sites/categories.json', '[' . json_encode($cats, JSON_FORCE_OBJECT) . ']');
	}
