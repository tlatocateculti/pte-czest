<?php 
	require_once('./utilityFunctions.php');
// 	require_once "../../scripts/database.php";
	require_once "../../config.php";
	require_once "../../scripts/utilityFunctions.php";
	require_once "../../scripts/search.php";
	confirmSession();
	
	function getCatPos(&$cats, &$fileKey, &$btnPos = '', &$catName = '') {
		if ($catName === '') {
			$catsName = array_keys($cats);
			for ($k = 0; $k < count($catsName); $k++) {
				$subCats = array_keys($cats[$catsName[$k]]);
				for ($i = 0; $i < count($subCats); $i++) {
					$btnPos = (isset($cats[$catsName[$k]][$subCats[$i]]['buttons']) ? @count($cats[$catsName[$k]][$subCats[$i]]['buttons']) : 0);
					for ($j = 0; $j < $btnPos; $j++) {
						if ($cats[$catsName[$k]][$subCats[$i]]['buttons'][$j]['site'] === $fileKey) {
							$btnPos = $j;
							$catName = $catsName[$i];
							return;
						}
					}
				}
			}
		}
		else {
			$btnPos = (isset($cats[$catName]['buttons']) ? @count($cats[$catName]['buttons']) : 0);
			for ($i = 0; $i < $btnPos; $i++) {
				if ($cats[$catName]['buttons'][$i]['site'] === $fileKey) {
					$btnPos = $i;
					return;
				}
			}
		}
// 		$catPos = count($cats);
// 		for ($i = 0; $i < $catPos; $i++) {
// 			if ($cats[$i]['name'] ===  $_POST['btncategory']) {
// 				$catPos = $i;
// 				break;
// 			}
// 		}
// 		$btnPos = (isset($cats[$catPos]['buttons']) ? @count($cats[$catPos]['buttons']) : 0);
		
		
	}
	
	if (isset($_POST['siteBelong'])) {
		$files = readEntries('../../json', 'sites.json');
		//print_r($_POST);

		$addArray = array('addDate' => $_POST['addDate'],
				'text' => $_POST['text'],
				'header' => $_POST['header'],
				'description' => $_POST['description'],
				'siteBelong' => $_POST['siteBelong'],
				'active' => $_POST['active'],
				'user_id' => $_POST['user_id']);
// 		if (isset($_POST['gallery'])) {
// 			$addArray['gallery'] = $_POST['gallery'];
// // 			for ($i = 0; count($_POST['gallery']); $i++) {
// // 				
// // 			}
// 		}
		if (isset($_POST['b64']))
			$addArray['b64'] = $_POST['b64'];
		$fileKey = $_POST['siteBelong'];
// 		if ($_POST['subsites'] == 1) {
// 			$fileKey = substr($_POST['siteBelong'], 4);
// 		}
// 		else {	
// 			$fileKey = substr($_POST['siteBelong'], 2);
// 		}
		$files[$fileKey] = $addArray;
		//print_r($files);
		file_put_contents('../../json/sites.json', '[' . json_encode($files, JSON_FORCE_OBJECT) . ']');
		unset($files);
		
// 		if ($_POST['mainbtncategory'] !== '' && $_POST['btncategory'] !== '' && $_POST['btnname'] !== '') {
// 			$cats = readEntries('../../json', 'categories.json');
// 			$btnPos ='';// = (isset($cats[$_POST['btncategory']]['buttons']) ? @count($cats[$_POST['btncategory']]['buttons']) : 0);
// 			getCatPos($cats[$_POST['mainbtncategory']],$fileKey,$btnPos,$_POST['btncategory']);
// 	// 		$catPos = count($cats);
// 	// 		for ($i = 0; $i < $catPos; $i++) {
// 	// 			if ($cats[$i]['name'] ===  $_POST['btncategory']) {
// 	// 				$catPos = $i;
// 	// 				break;
// 	// 			}
// 	// 		}
// 	// 		$btnPos = (isset($cats[$catPos]['buttons']) ? @count($cats[$catPos]['buttons']) : 0);
// 	// 		
// 	// 		for ($i = 0; $i < $btnPos; $i++) {
// 	// 			if ($cats[$catPos]['buttons'][$i]['site'] === $fileKey) {
// 	// 				$btnPos = $i;
// 	// 				break;
// 	// 			}
// 	// 		}
// 			
// 			$addArray = array('name' => $_POST['btnname'],
// 					'position' => $btnPos,
// 					'site' => $fileKey,
// 					'siteAddress' => $_POST['siteBelong'],
// 					'active' => $_POST['active']);
// 					//print_r($btnPos);
// 			$cats[$_POST['mainbtncategory']][$_POST['btncategory']]['buttons'][$btnPos] = $addArray;
// 			//print_r($cats);
// 			file_put_contents('../../json/categories.json', '[' . json_encode($cats, JSON_FORCE_OBJECT) . ']');
// 		}
	}
	
	if (isset($_POST['newsHeader'])) {
		$files = readEntries('../../json', '/news.json');
		print_r($_POST);

		$addArray = array('addDate' => $_POST['addDate'],
				'text' => $_POST['text'],
				'newsHeader' => $_POST['newsHeader'],
				'description' => $_POST['description'],
				'shortText' => $_POST['shortText'],
				'active' => $_POST['active'],
				'user_id' => $_POST['user_id']);
		if (isset($_POST['gallery'])) {
			$addArray['gallery'] = $_POST['gallery'];
// 			for ($i = 0; count($_POST['gallery']); $i++) {
// 				
// 			}
		}
		if (isset($_POST['b64']))
			$addArray['b64'] = $_POST['b64'];
		if (isset($_POST['site_id']) && $_POST['site_id'] !== '') 
			$fileKey = (int)$_POST['site_id'];
		else 
			$fileKey = count($files);
// 		if ($_POST['subsites'] == 1) {
// 			$fileKey = substr($_POST['siteBelong'], 4);
// 		}
// 		else {	
// 			$fileKey = substr($_POST['siteBelong'], 2);
// 		}
		$files[$fileKey] = $addArray;
		print_r($files);
		file_put_contents('../../json/news.json', '[' . json_encode($files, JSON_FORCE_OBJECT) . ']');
		unset($files);
	}	
	if (isset($_POST['hideID'])) {
 		if(isset($_GET['s'])) {
			$files = readEntries('../../json', 'sites.json');
			$files[$_POST['hideID']]['active'] = $_POST['hideValue'];
			file_put_contents('../../json/sites.json', '[' . json_encode($files, JSON_FORCE_OBJECT) . ']');
		}
		else {
			$files = readEntries('../../json', 'news.json');
			$files[$_POST['hideID']]['active'] = $_POST['hideValue'];
			file_put_contents('../../json/news.json', '[' . json_encode($files, JSON_FORCE_OBJECT) . ']');
		}
		echo $_POST['hideValue'];
	}
	if (isset($_POST['deleteId'])) {
		if(isset($_GET['s'])) {
			$files = readEntries('../../json', 'sites.json');
			unset($files[$_POST['deleteId']]);
			//$files = array_values($files);
			file_put_contents('../../json/sites.json', '[' . json_encode($files, JSON_FORCE_OBJECT) . ']');
		}
		else {
			$files = readEntries('../../json', 'news.json');
			unset($files[$_POST['deleteId']]);
			print_r($files);
			$files = array_values($files);
			print_r($files);
			file_put_contents('../../json/news.json', '[' . json_encode($files, JSON_FORCE_OBJECT) . ']');
		}
		unset($files);
// 		$cats = readEntries('../../json', 'categories.json');
// 		$catPos = '';
// 		$btnPos = 0;
// 		getCatPos($cats,$_POST['deleteId'],$btnPos,$catPos);
// 		unset($cats[$catPos]['buttons'][$btnPos]);
// 		if (count($cats[$catPos]['buttons'])) unset($cats[$catPos]);
// 		file_put_contents('../../json/categories.json', '[' . json_encode($cats, JSON_FORCE_OBJECT) . ']');
	}
