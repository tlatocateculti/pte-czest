function createLocalizationPanel() {
	var address = "./phps/localizations_maintenance.php";
	adminCommunication(address, '', '', 
			   function () {
				   document.getElementById("localizationContent").innerHTML = 'Ładuję dane...';
			   },
		    function(r) {
			    //console.log('Jestem w funkcji, dane to: ' + r);
			    document.getElementById("localizationContent").innerHTML = r;
			    //tableMaintenace();
			    //document.getElementById('actionButtons').style.display = 'block';
		    });	
}

function openWindow(n) {
	var w;
	if (n === undefined) 
		n = 0;
	if (n == 1) {
		w = document.getElementById('localizationWindows');
		w.style.display = 'block';
		w.style.width = (w.parentNode.offsetWidth - 100) + 'px';
		w.style.height = (w.parentNode.offsetHeight - 100) + 'px';
		w.style.right = '50px';
		w.style.top = '50px';
		document.getElementById('hoursRange').innerHTML = '';
		document.getElementsByName('hourStart')[0].children[0].selected = 'selected';
		document.getElementsByName('minStart')[0].children[0].selected = 'selected';
		document.getElementsByName('hourStop')[0].children[0].selected = 'selected';
		document.getElementsByName('minStop')[0].children[0].selected = 'selected';
		var inputs = w.getElementsByTagName('input');
		for (var i = 0; i < inputs.length; i++)
			inputs[i].checked = false;
	}
	if (n == 0) {
		w = document.getElementById('telephonesWindow');
		w.style.display = 'block';
		w.style.width = (w.parentNode.offsetWidth - 100) + 'px';
		w.style.height = (w.parentNode.offsetHeight - 100) + 'px';
		w.style.right = '50px';
		w.style.top = '50px';
		document.getElementsByName('telType')[0].children[0].selected = 'selected';
		generateTelForm();
	}
}

function deleteSelectedLocalization(v, t) {
	if (t === undefined) return;
	while (t.className.indexOf('settingsBody') == -1 && t.className.indexOf('headerSetName') == -1 ) {
		if (t.parentNode) 
			t = t.parentNode; 
		else return;
		if (t.className === undefined) t.className = '';
		//console.log(t);
	}
	if (t.parentNode.lastChild.id === 'siteEditDiv') {
		t.parentNode.lastChild.style.display = '';
		t.parentNode.removeChild(t.parentNode.lastChild);
		return;
	}
// 	var rows = document.getElementById('localizationsTable').getElementsByTagName('tbody')[0].children;
// 	var rowsToDel = [];
	var sendData = 'remove=';
	if (v !== undefined && v == -1)
		sendData += '2';
	else
		sendData += '1';
	sendData += '&id=' + t.dataset.id;
// 	for (var i = 0; i < rows.length; i++) {
// 		console.log(rows[i].children[rows[i].children.length - 1].children[0].checked);
// 		if (rows[i].children[rows[i].children.length - 1].children[0].checked) {
// 			sendData += 'id[' + rowsToDel.length + ']=' + rows[i].children[rows[i].children.length - 1].children[0].value + '&';
// 			rowsToDel[rowsToDel.length] = rows[i];//.children[rows[i].children.length - 1].children[0];				
// 		}
// 	}
// 	sendData = sendData.slice(0, sendData.length - 1);
// 	console.log(sendData);
	adminCommunication('./phps/localizations_save.php', sendData, '', '',
				function(r) {
// 				if (r == 'delOK') {
					createLocalizationPanel();
// 					for (var i = 0; i < rowsToDel.length; i++) 
// 						rowsToDel[i].remove();
// 				}
				});
	
}

//kompletnie nie działa -  do poprawy!!!!
function addNewReception(t) {
	var rhl = document.getElementById('localizationHoursList');
	var hourRangeP = document.getElementById('hoursRange').getElementsByTagName('p');
	var w = document.getElementById('localizationWindows');
	var inputs = w.getElementsByTagName('input');
	
	var days = [];
	for (var i = 0; i < inputs.length; i++) {
		if(inputs[i].checked) 
			days[days.length] = parseInt(inputs[i].value);			
	}
	var paragraphHoursListCount = rhl.getElementsByTagName('p').length;
	var k = 0;
	var lastDayId = -1;
	var sep = false;	//jak separator
	var dayStr = '';//'<p id="date' + k + '">';
	var tmpStr = '';
	var availableHoursStr = '';
	var parElem = document.createElement('p');
	parElem.id = 'dateList-' + paragraphHoursListCount++;
	
	for (var j = 0; j < hourRangeP.length; j++)
		availableHoursStr += hourRangeP[j].innerHTML.slice(0, hourRangeP[j].innerHTML.indexOf('<')) + ',';
	availableHoursStr = availableHoursStr.slice(0, availableHoursStr.length - 1);
	while (k < days.length) {
		if ((lastDayId + 1) == days[k] && lastDayId !== -1 && !sep) {
			dayStr += tmpStr + ' - ';
			sep = true;
		}
		else if (lastDayId !== -1 && (lastDayId + 1) !== days[k]){
			sep = false;
			dayStr += tmpStr + ': ' + availableHoursStr + "<button onclick=\"removeP(this);\">Usuń</button>";
			parElem.innerHTML = dayStr;
			dayStr = '';
			rhl.appendChild(parElem);
			var parElem = document.createElement('p');
			parElem.id = 'dateList-' + paragraphHoursListCount++;
			//hourStr += tmpStr + '<button>Usuń</button></p><p id="date' + k + '">';
			tmpStr = '';
		}
		switch(days[k]) {
			case 0: tmpStr = 'Poniedziałek'; break;
			case 1: tmpStr = 'Wtorek'; break;
			case 2: tmpStr = 'Środa'; break;
			case 3: tmpStr = 'Czwartek'; break;
			case 4: tmpStr = 'Piątek'; break;
			case 5: tmpStr = 'Sobota'; break;
			case 6: tmpStr = 'Niedziela'; break;
		}
		lastDayId = days[k++];
	}
	if (tmpStr !== '')
		dayStr += tmpStr + ': ' + availableHoursStr;
	dayStr += "<button onclick=\"removeP(this);\">Usuń</button>";
	//dayStr += "<button onclick=\"delete('" + parElem.id + "');\">Usuń</button>";
	parElem.innerHTML = dayStr;
	rhl.appendChild(parElem);
	//rhl.innerHTML += hourStr + '<button>Usuń</button></p>';
	closeWindow(t);
}



function removeP(t) {
	t.parentNode.remove();
}

function addHourRange() {
	var singleHour = false;
	var hrHTML = document.getElementById('hoursRange');
	var pCount = hrHTML.getElementsByTagName('p').length;
	var hs = document.getElementsByName('hourStart')[0].value;
	var is = document.getElementsByName('minStart')[0].value;
	var he = document.getElementsByName('hourStop')[0].value;
	var ie = document.getElementsByName('minStop')[0].value;
	if (hs === he && is === ie)
		singleHour = true;
	if (hs < 10)
		hs = '0' + hs;		
	if (is < 10)
		is = '0' + is;
	if (!singleHour) {
		if (he < 10)
			he = '0' + he;
		if (ie < 10)
			ie = '0' + ie;
	}
	var p = '<p id="p-' + pCount + '">' + hs + ':' + is;
	if (!singleHour)
		p += '-' + he + ':' + ie;
	p += "<button onclick=\"removeP(this)\">Usuń</button></p>";
	hrHTML.innerHTML += p
}

function addNewTelephone(t) {
	var telType = parseInt(document.getElementsByName('telType')[0].value);
	var telDiv = document.getElementById('telephonesList');
	var newItem = document.createElement('p');
	var contentStr = ''
	if (telType == 0) {
		contentStr += 'Telefon komórkowy: (+' + 
				document.getElementsByName('prefix')[0].value + ') ' + 
				document.getElementsByName('telephone')[0].value /*+
				"<button onclick='removeP(this);'>Usuń</button>"*/;
	}
	else if (telType == 1) {
		contentStr += 'Telefon stacjonarny: (+' + 
				document.getElementsByName('prefix')[0].value + ') ' + 
				document.getElementsByName('prefixTown')[0].value + 
				document.getElementsByName('telephone')[0].value /*+
				"<button onclick='removeP(this);'>Usuń</button>"*/;
	}
	else if (telType == 3) {
		contentStr += 'Fax: (+' + 
				document.getElementsByName('prefix')[0].value + ') ' + 
				document.getElementsByName('prefixTown')[0].value + 
				document.getElementsByName('telephone')[0].value /*+
				"<button onclick='removeP(this);'>Usuń</button>"*/;
	}
	else if (telType == 5) {
		contentStr += 'Email: ' + document.getElementsByName('email')[0].value /*+
				"<button onclick='removeP(this);'>Usuń</button>"*/;
	}
	else if (telType == 4) {
		contentStr += 'Telefon/fax: (+' + 
				document.getElementsByName('prefix')[0].value + ') ' + 
				document.getElementsByName('prefixTown')[0].value + 
				document.getElementsByName('telephone')[0].value /*+
				"<button onclick='removeP(this);'>Usuń</button>"*/;
	}
	else if (telType == 2) {
		contentStr += document.getElementsByName('netService')[0].value + ': ' +
				document.getElementsByName('netlogin')[0].value/* +
				"<button onclick='removeP(this);'>Usuń</button>"*/;
	}
	if (document.getElementsByName('telcomment')[0].value) contentStr += ', ' + document.getElementsByName('telcomment')[0].value;
	contentStr += "<button onclick='removeP(this);'>Usuń</button>";
	newItem.innerHTML = contentStr;
	telDiv.appendChild(newItem);
	closeWindow(t);
}

function generateTelForm(t) {
	var telType = 0;
	if (t)
		telType = parseInt(t.value);
	var form = document.getElementById('telephoneForm');
	var formInput = '';
	if (telType == 0) {
		formInput += "<div><span>Kierunkowy kraju</span><input type='text' name='prefix' placeholder='Format np.: xx' value='48'/></div>";
		formInput += "<div><span>Numer telefonu</span><input type='text' name='telephone' placeholder='Format: xxx-xxx-xxx'/></div>";
	}
	else if (telType == 1 || telType == 3 || telType == 4) {
		formInput += "<div><span>Kierunkowy kraju</span><input type='text' name='prefix' placeholder='Format np.: xx' value='48'/></div>";
		formInput += "<div><span>Kierunkowy miasta</span><input type='text' name='prefixTown' placeholder='Format: xx'/></div>";
		formInput += "<div><span>Numer telefonu</span><input type='text' name='telephone' placeholder='Format: x-xxx-xxx'/></div>";
	}
	else if (telType == 5) {
		formInput += "<div><span>Adres poczty elektronicznej</span><input type='text' name='email' placeholder='Format: nazwa@serwer.doemna'/></div>";
	}
	else if (telType == 2) {
		formInput += "<div><span>Nazwa usługi</span><input type='text' name='netService' placeholder='Format: nazwa (np. Skype)'/></div>";
		formInput += "<div><span>Nazwa użytkownika/numer</span><input type='text' name='netlogin' placeholder=''/></div>";
	}
	formInput += "<div><span>Opcjonalny komentarz (np. imię i nazwisko pracownika)</span><input type='text' name='telcomment' placeholder=''/></div>";
	form.innerHTML = formInput;
}

function getmapCoords(data) {
	var coords;
	if (data.indexOf('google') != -1 && data.indexOf('maps') != -1) {
		data = data.split('/');
		for (var i = 0; i < data.length; i++) {
			if (data[i].indexOf('@') != -1) {
				//data = data.split(',');
				coords = data[i].slice(1, data[i].length - 1);
				return coords;
			}
		}
		//return false;
	}
	else if (data.indexOf('here') !== -1) {
		data = data.split('?')[1].split('&');
		for (var i = 0; i < data.length; i++) {
			if (data[i].indexOf('map') != -1) {
				data = data[i].split('=')[1].split(',');
				coords = data[0] + ',' + data[1] + ',' + data[2];
				return coords;
			}
		}
		//return false;
	}
	else if (data.indexOf('openstreetmap') !== -1) {
		data = data.split('#')[1].split('=')[1].split('/');
		if (data.length == 3) {
			coords = data[1] + ',' + data[2] + ',' + data[0];
			return coords;
		}
		//return false;
	}
	data = data.split(',');
	if (/*data.length == 2 || */data.length == 3) {
		return coords;
	}
	return false;
}

function saveLocalization() {
	var form = document.getElementById('localizationForm');
	var sendData = '';
	var telephones = '';
	var telDiv = document.getElementById('telephonesList');
	var recVisits = '';
	var recDiv = document.getElementById('localizationHoursList');
	var street = '';
	street += document.getElementsByName('streetprefix')[0].value + ';;' +
			document.getElementsByName('street')[0].value + ';;' + 
			document.getElementsByName('placenum')[0].value + ';;' +
			document.getElementsByName('localnum')[0].value;
	var i;
	//console.log(telDiv.children);
	for (i = 0; i < telDiv.children.length; i++) {
		if (telDiv.children[i].children[0])
			telDiv.children[i].children[0].remove();
		//ponizsze działanie zapobiega wyrzuceniu przesyłania znaku + (podczas przesłania traktowany
		//jako operator, nie jako znak!)
		var tel = telDiv.children[i].innerHTML.replace('+', '--');
		telephones += tel/*Div.children[i].innerHTML*/ + ';;';
	}
	telephones = telephones.slice(0, telephones.length - 2);
	for (i = 0; i < recDiv.children.length; i++) {
		if (recDiv.children[i].children[0])
			recDiv.children[i].children[0].remove();			
		recVisits += recDiv.children[i].innerHTML + ';;';
	}
	recVisits = recVisits.slice(0, recVisits.length - 2);
	sendData += 'addressname=' + document.getElementsByName('addressname')[0].value + '&';
	sendData += 'comment=' + document.getElementsByName('comment')[0].value + '&';
	sendData += 'address=' + street + '&';
	sendData += 'town=' + document.getElementsByName('town')[0].value + '&';
	sendData += 'postalcode=' + document.getElementsByName('postalcode')[0].value + '&';
	sendData += 'postoffice=' + document.getElementsByName('postoffice')[0].value + '&';
	sendData += 'telephones=' + telephones + '&';
	sendData += 'workHours=' + recVisits + '&';
	sendData += 'idEdit=' +  document.getElementsByName('idEdit')[0].value + '&';
	sendData += 'lat=' +  document.getElementsByName('lat')[0].value + '&';
	sendData += 'lon=' +  document.getElementsByName('lon')[0].value + '&';
	sendData += 'zoom=' +  document.getElementsByName('zoom')[0].value + '&';
	sendData += 'deleted=0&';
	adminCommunication('./phps/localizations_save.php', sendData, '', '',
				function(r) {
					if (document.getElementsByName('idEdit')[0].value != -1)
						document.getElementById("localizationContent").innerHTML = "Pomyślnie zaktualizowano dane o lokalizacji!";
					else
						document.getElementById("localizationContent").innerHTML = "Pomyślnie dodano nową lokalizację!";
					setTimeout(createLocalizationPanel, 1000); 
				});
}

function editSelectedClinic(t) {
	if (t.dataset.selectNull == 0 || t.dataset.selectNull === undefined) {
		console.log(t.children[1].innerHTML + ' ' + t.dataset.selectNull);
		if (t.children[t.children.length - 1].children[0].checked == '')
			t.children[t.children.length - 1].children[0].checked = 'checked';
		else 
			t.children[t.children.length - 1].children[0].checked = '';
	}
	else {
		var obj = t;
		var rows = t.parentNode.children;
		for (var i = 0; i < rows.length; i++) {
			rows[i].children[rows[i].children.length - 1].children[0].checked = '';
		}			
		if (t.type !== 'checkbox')
			obj = t.children[t.children.length - 1].children[0];
		obj.checked = 'checked';
		loadForm(obj);
	}
}

//poprawic wczytywanie!!
function loadForm(obj) {
	clearMainForm();
	document.getElementsByName('name')[0].value = obj.parentNode.parentNode.children[1].innerHTML;
	document.getElementsByName('comment')[0].value = obj.parentNode.children[1].value;
	var street = obj.parentNode.children[2].value.split(';;');
	document.getElementsByName('streetPrefix')[0].children[street[0]].selected = 'selected';
	document.getElementsByName('streetName')[0].value = street[1];
	if (street.length > 2)
		document.getElementsByName('streetNum')[0].value = street[2];
	if (street.length > 3)
		document.getElementsByName('streetLoc')[0].value = street[3];	
	document.getElementsByName('city')[0].value = obj.parentNode.parentNode.children[3].innerHTML;
	document.getElementsByName('city_code')[0].value = obj.parentNode.children[3].value;
	var tels = obj.parentNode.children[4].value.split(';;');
	var telDiv = document.getElementById('telephonesList');
	//telDiv.innerHTML = '';
	for (var i = 0; i < tels.length; i++) {
		telDiv.innerHTML += '<p>' + tels[i] + "<button onclick='removeP(this);'>Usuń</button></p>";
	}
	console.log(obj.parentNode.children[5].value);
	var recHours = obj.parentNode.children[5].value.split(';;');
	console.log(recHours);
	var recDiv = document.getElementById('localizationHoursList');
	//recDiv.innerHTML = '';
	for (var i = 0; i < recHours.length; i++) {
		recDiv.innerHTML += '<p>' + recHours[i] + "<button onclick='removeP(this);'>Usuń</button></p>";
		//<button onclick='removeP(this);'>Usuń</button>
	}
	document.getElementsByName('map_geo')[0].value = obj.parentNode.children[6].value;
	document.getElementsByName('idEdit')[0].value = obj.value;
}

function localizationAddEdit(edit, t) {
	if (t === undefined) return;
	while (t.className.indexOf('settingsBody') == -1 && t.className.indexOf('headerSetName') == -1 ) {
		if (t.parentNode) 
			t = t.parentNode; 
		else return;
		if (t.className === undefined) t.className = '';
		//console.log(t);
	}
	if (t.parentNode.lastChild.id === 'siteEditDiv') {
		t.parentNode.lastChild.style.display = '';
		t.parentNode.removeChild(t.parentNode.lastChild);
		return;
	}
	var sendData = '';
	var address = "./phps/localizations_addedit.php";
	var tels, recHours;
	if (edit !== undefined && edit) {
		sendData += 'fields=' + t.dataset.id + '&';
		sendData += 'addressname=' + t.dataset.addressname + '&';
		sendData += 'comment=' + t.dataset.comment + '&';
		sendData += 'streetprefix=' + t.dataset.streetprefix + '&';
		sendData += 'street=' + t.dataset.street + '&';
		sendData += 'placenum=' + t.dataset.placenum + '&';
		sendData += 'localnum=' + t.dataset.localnum + '&';
		sendData += 'town=' + t.dataset.town + '&';
		sendData += 'postalcode=' + t.dataset.postalcode + '&';
		sendData += 'postoffice=' + t.dataset.postoffice + '&';
// 		sendData += 'telephones=' + t.dataset.tels + '&';
// 		sendData += 'workHours=' + t.dataset.workHours + '&';
		sendData += 'lat=' +  t.dataset.lat + '&';
		sendData += 'lon=' +  t.dataset.lon + '&';
		sendData += 'zoom=' +  t.dataset.zoom + '&';
		sendData += 'deleted=' + t.dataset.deleted;
		tels = t.dataset.tels;
		recHours = t.dataset.opens;
	}
	adminCommunication(address, sendData, '', '',
			   function(r) {
				   if (r == -100) {
					   document.getElementById("localizationContent").innerHTML = "Błąd! Nic nie zaznaczyłeś!";
					   setTimeout(createLocalizationPanel, 1000);  
					   return;
				   }
				   var d = document.getElementById('siteEditDiv');
				   if (d === undefined || d === null) d = document.createElement('div');
				   d.id = 'siteEditDiv';				   
				   d.style.display = 'block';
				   d.innerHTML = r;				   
				   t.parentNode.appendChild(d);
				   if (edit !== undefined && edit) {
					tels = tels.split(';;');
					var telDiv = document.getElementById('telephonesList');
					telDiv.innerHTML = '';
					for (var i = 0; i < tels.length; i++) {
						if (tels[i] === '' || tels[i] === undefined) continue;
						telDiv.innerHTML += '<p>' + tels[i] + "<button onclick='removeP(this);'>Usuń</button></p>";
					}
					//console.log(obj.parentNode.children[5].value);
					recHours = recHours.split(';;');
					//console.log(recHours);
					var recDiv = document.getElementById('localizationHoursList');
					recDiv.innerHTML = '';
					for (var i = 0; i < recHours.length; i++) {
						if (recHours[i] === '' || recHours[i] === undefined) continue;
						recDiv.innerHTML += '<p>' + recHours[i] + "<button onclick='removeP(this);'>Usuń</button></p>";
						//<button onclick='removeP(this);'>Usuń</button>
					}   
				   }   
				   showOnOSMMap("gmap_canvas", 0, 0, 5);
				   var names = ['street', 'placenum', 'localnum', 'postalcode', 'town', 'mapzoom'];
				   var postalcode = document.getElementsByName('postalcode')[0];
				   var country = document.getElementsByName('country')[0];
				   var postoffice = document.getElementsByName('postoffice')[0];
				   var town = document.getElementsByName('town')[0];
				   var maplink = document.getElementsByName('maplink')[0];
				   var street = document.getElementsByName('street')[0];
				   names.forEach(function(item, i) {
					   var s = document.getElementsByName(item)[0];
					   if (s !== undefined) {
						s.addEventListener('change', function() {
							if (maplink.value === '' || maplink.value === undefined)
								findOSMMap(document.getElementsByName('placenum')[0].value + ' ' + document.getElementsByName('street')[0].value,  town.value, postalcode.value, country.value, document.getElementsByName('mapzoom')[0].value, function(r, lat, lon, z) {
									if (r === undefined) return;
									var city =  r["city"] || r["village"];
									var st = r["road"] || r["pedestrian"];
									if (r["postcode"] !== undefined && r["postcode"] !== "undefined" && town.value !== '')
										postalcode.value = r["postcode"];
									if (country.value === '' && r["country"] !== undefined && r["country"] !== "undefined")
										country.value = r["country"];
									if ((postoffice.value === '' && r["county"] !== undefined && r["county"] !== "undefined") || r["county"] !== postoffice.value)
										postoffice.value = r["county"];
									if (town.value === '' && city !== undefined && city !== "undefined" && postalcode.value !== '')
										town.value = city;
									if (street.value !== st && st !== undefined)
										street.value = st;
									if (town.value != city && city !== undefined && city !== 'undefined' )
										town.value = city;
									document.getElementsByName('lat')[0].value = lat;
									document.getElementsByName('lon')[0].value = lon;
									document.getElementsByName('zoom')[0].value = z;
								});
						}); 
// 						if (item === 'mapzoom') 
// 							s.addEventListener('change', function() {
					   }
				   });
				   if (edit !== undefined && edit) {
					document.getElementsByName('mapzoom')[0].dispatchEvent(new Event('change'));
				   }
				   maplink.addEventListener('change', function() {
					var c = getmapCoords(maplink.value);
					if (!c) return false;
					c = c.split(',');
					revFindOSMMap(c[0], c[1], c[2], function(r, lat, lon, z) {
						if (r === undefined) return;
						var city =  r["city"] || r["village"];
						var st = r["road"] || r["pedestrian"];
						street.value = st;
						if (r["house_number"] !== undefined || r["house_number"] !== 'undefined')
							document.getElementsByName('placenum')[0].value = r["house_number"];
						postalcode.value = r["postcode"];
						town.value = city;
						postoffice.value = r["county"];
						country.value = r["country"];
						document.getElementsByName('lat')[0].value = lat;
						document.getElementsByName('lon')[0].value = lon;
						document.getElementsByName('zoom')[0].value = z;
					});
				   });
			}); 
}

function clearMainForm() {
	document.getElementsByName('name')[0].value = '';
	document.getElementsByName('comment')[0].value = '';
	document.getElementsByName('streetPrefix')[0].children[0].selected = 'selected';
	document.getElementsByName('streetName')[0].value = '';
	document.getElementsByName('streetNum')[0].value = '';
	document.getElementsByName('streetLoc')[0].value = '';
	document.getElementsByName('city')[0].value = '';
	document.getElementsByName('city_code')[0].value = '';
	document.getElementsByName('map_geo')[0].value = '';
	document.getElementById('telephonesList').innerHTML = '';
	document.getElementById('localizationHoursList').innerHTML = '';
	document.getElementsByName('idEdit')[0].value = -1;		
}