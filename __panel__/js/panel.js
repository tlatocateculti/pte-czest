function logout() {
	window.location = "?showLogout";
	//adminCommunication('', '', 'showLogout', '', function() {window.location.reload();});
}

function openDetails(num) {
	if (num === undefined) return;
	var sd = document.getElementById('settingsDiv');
	if (sd === undefined) return;
	sd.style.display = 'block';
	if (num == 0) {
		adminCommunication('./phps/localizations.php', '', '', '', function(r) {
			document.getElementById('windowBody').innerHTML = r;
			createLocalizationPanel();
		});
	}
	else if (num == 1 || num == 2) {
		adminCommunication('./phps/sites.php', '', '', '', function(r) {
			document.getElementById('windowBody').innerHTML = r;
			if (num == 1) getSitesTable();
			if (num == 2) getSitesTable(0, true);
			//getCategories();
		});
	}
// 	else if (num == 3 || num == 4) {
// 		adminCommunication('./phps/courses.php', '', '', '', function(r) {
// 			document.getElementById('windowBody').innerHTML = r;
// 			if (num == 3) getCoursesTable();
// 			if (num == 4) getCoursesTable(0, true);
// 			//getCategories();
// 		});
// 	}
	else if (num == 5) {
		adminCommunication('./phps/categories.php', '', '', '', function(r) {
			document.getElementById('windowBody').innerHTML = r;
			getCategories();
		});
	}
	else if (num == 6) {
		adminCommunication('./phps/galleries.php', '', '', '', function(r) {
			document.getElementById('windowBody').innerHTML = r;
			getGalleriesTable(true);
		});
	}
	else if (num == 7) {
		adminCommunication('./phps/settings.php', '', '', '', function(r) {
			document.getElementById('windowBody').innerHTML = r;
		});
	}
}

function closeDetails(t) {
	
	if (this !== undefined && t === undefined) t = this;
	while(t) {
		console.log(t);
		if (t.id) if (t.id === 'settingsDiv') {t.style.display = ''; break;}
		t = t.parentNode;
	}
}