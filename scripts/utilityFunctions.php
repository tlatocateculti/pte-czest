<?php
	//error_reporting(E_ALL);
	require_once(__DIR__ . '/../config.php');
	//funkcja ma na celu wypisanie wskazanej ilości elementów z podanej tablicy jako ciąg znakowy
	//Wejście:
	//$a - tablica wejściowa
	//$s - numer indeksu startu (domyślnie 0, czyli pierwszy element)
	//$h - numer elementu końcowego LUB ilość elementów do wyświetlenia (pomniejszona o wskazaną ilość);
	//     domyślnie 0, wtedy funkcja pobiera elementy z ostatnim włącznie
	//     wartości dodatnie to numer końcowy (np. 5 to wyciągnięcie wartości WŁĄCZNIE z indeksu 5)
	//     wartości ujemne to ilość elementów do wyświetlenia (np. -5 oznacza, że od wartości $s zostaną wyświetlone wszystkie elementy poza ostatnimi 5)
	//     funkcja posiada zabezpieczenia - wyświetli albo wszystkie elementy albo żadnego (wartości ujemne)
	//$c - znak oddzielający człony nowego wyrazu; domyślnie pusty znak (spacja)
	//Wyjście:
	//zwrócony zostaje odpowiedni ciąg znakowy
	function print_array_values($a, $s = 0, $h = 0, $c = ' ') {
		$tmp = count($a);
		if ($h == 0) $h = $tmp;
		if ($h > $tmp) $h = $tmp;
		if ($h < 0 && ( ($tmp + $h) > 0 )) $h = $tmp + $h;
		if ($h == 0) return '';
		$tmp = '';
		for ($s; $s < $h; $s++)
			$tmp .= $a[$s] . $c;
		return substr($tmp, 0, -1);
	}
	
	//funkcja odpowiada za przesyłanie poczty na wskazany adres; jako uwierzytelniający serwer poczty
	//wykorzystywany jest podany w konfiguracji (plik config_mail.php)
	//Wejście:
// 	//$sender - adres poczty osoby, która wysyła nam wiadomość
	//$nameName - nazwa osoby, któa wysyła nam wiadomość (może być tożsama z adresem poczty)
	//$receiver - do kogo kierowana jest wiadomość; może to być dowolny adres poczty (nawet nie związany z serwer wysyłającym pocztę)
	//$subject - temat naszej wysyłanej poczty
	//$message - treść wysyłanej wiadomości 
	//$contetnType - jakiego typu zawartość będzie przesyłana w wiadomości; domyślnie przesyłany jest prosty tekst, może być także
	//html czy też base 64 (załączniki)
	//WYJŚCIE:
	//informacja czy wiadomość została wysłana (1) czy też nie (inny numer, np. -1).
	function sendMail($sender, $nameName, $receiver, $subject, $message, $contentType = 'text/plain; charset=UTF-8') {
		$path = __DIR__ . '/../libs';
		set_include_path(get_include_path() . PATH_SEPARATOR . $path);
		require_once "Mail.php";;
		require_once __DIR__ . "/../config_mail.php";
		//require_once "utilityFunctions.php";
		if ($sender === '') $sender = decodePhrase(MAIL_LOG);
		$from = "=?utf-8?B?".base64_encode($nameName)."?=" . " <" . $sender . ">";
		//$from = $nameName . " <" . $sender . ">";
		$to = $receiver;
		$headers = array ('From' => $from,
		'To' => $to,
		'Subject' => "=?utf-8?B?".base64_encode($subject)."?=",
		//'Subject' => $subject,
		'Content-Type'  => $contentType);
		//echo "TUTAJ !!!!!" . decodePhrase(MAIL_SERVER) . " " . decodePhrase(MAIL_PORT) . " " .  decodePhrase(MAIL_LOG) . " " . decodePhrase(MAIL_PASS);
		$smtp = @Mail::factory('smtp', array ('host' => decodePhrase(MAIL_SERVER),
							'port' => decodePhrase(MAIL_PORT),
							'auth' => true,
							'username' => decodePhrase(MAIL_LOG),
							'password' => decodePhrase(MAIL_PASS)));
		//$smtp->enableCrypto(false);
		$mail = $smtp->send($to, $headers, $message);
		return $mail;
	}
	
	function sendMailAttachment($sender, $nameName, $receiver, $subject, $message, $file, $contentType = 'text/plain; charset=UTF-8') {
		$path = __DIR__ . '/../libs';
		set_include_path(get_include_path() . PATH_SEPARATOR . $path);
		require_once "Mail.php";
		require_once "Mail/mime.php";
		require_once __DIR__ . "/../config_mail.php";
		//require_once "utilityFunctions.php";
		if ($sender === '') $sender = decodePhrase(MAIL_LOG);
		$from = "=?utf-8?B?".base64_encode($nameName)."?=" . " <" . $sender . ">";
		$to = $receiver;
		
		$headers = array ('From' => $from,
		'To' => $to,
		'Subject' => "=?utf-8?B?".base64_encode($subject)."?=",
		'Content-Type'  => $contentType);
		
		$mime_params = array(
			'text_encoding' => '7bit',
			'text_charset' => 'UTF-8',
			'html_charset' => 'UTF-8',
			'head_charset' => 'UTF-8'
		);
		
		$mime = new Mail_mime("\n");
		$mime->setTXTBody($message);
		for ($i = 0; $i < count($file); $i++)
			$mime->addAttachment($file[$i]);
		
		$message = $mime->get($mime_params);
		$headers = $mime->headers($headers);
		
		$smtp = @Mail::factory('smtp', array ('host' => decodePhrase(MAIL_SERVER),
							'port' => decodePhrase(MAIL_PORT),
							'auth' => true,
							'username' => decodePhrase(MAIL_LOG),
							'password' => decodePhrase(MAIL_PASS)));
		//$smtp->enableCrypto(false);
		$mail = $smtp->send($to, $headers, $message);
		
		//removeDir($file[0]);
		return $mail;
	}
	
	//funkcja pobiera i wrzuca do tablicy zmienne pobrane z adresu WWW (jako GET prywatne)
	//Wejście:
	//$c - separator zmiennych z zapytania; domyślnie znak '/'
	//$k - jeżeli potrzeba, można zmienne przypisać pod konkretne klucze
	//     jeżeli kluczy będzie za mało, wartości zostaną dopisane do liczb
	// dorobić możliwość robijania zmiennych po znaku specjalnym!! (np. m_strona -> $_PRIV_GET['m'] = 'strona';)
	function getGET($c = '/', $k = '') {
		global $_PRIV_GET;
		if (is_array($k)) {
			$tmp = explode($c, $_SERVER['QUERY_STRING']);
			if (count($k) < count($tmp))
				
			$i = 0;
			foreach($tmp as $e) {
				if ($i == count($k))
					break;
				$_PRIV_GET[ $k[$i++] ] = $e;
			}
			for ($i; $i < count($tmp); $i++)
				$_PRIV_GET[$i] = $tmp[$i];
			return;
		}
		$_PRIV_GET = explode($c, $_SERVER['QUERY_STRING']);
	}
	
	//funkcja koduje podany ciąg znakowy/bajtowy
	function encodePhrase($s) {
		return gzdeflate(base64_encode($s));
	}

	//funkcja dekoduje podany ciąg znakowy/bajtowy
	function decodePhrase($s) {
		return base64_decode(gzinflate($s));
	}
	
	function getSettingEmail() {
		//print_r(__DIR__ . '/../data/email.dat');
		if ($f = @fopen(__DIR__ . '/../data/email.dat', 'r')) { 
			$sRead = fread($f, filesize(__DIR__ . "/../data/email.dat"));
			fclose($f);
			return decodePhrase($sRead);
		}
		else
			return false;
		
	}
	
	//funkcja przeszukuje podany ciąg znakowy pod względem wystąpienia w nim 
	//określonych fragmentów tekstu; jak wynik zwracana jest tablica, w której podane
	//są podtablice zawierające pozycję znalezienia oraz fragment, który został znaleziony
	//$f - przeszukiwany ciąg/tablica ciągu
	//$a - tablica fragmentów do znalezienia w ciągu
	//$o - przesunięcie (od której pozycji ma być przeszukiwany ciąg, domyślnie 0)
	function findPosArray($f, $a, $o = 0) {
		if (!is_array($a)) return false;
		$r = [];
		$fArray = is_array($f);
		for($i = 0; $i < count($a); $i++) {
			if ($fArray) {
				for ($j = 0; $j < count($f); $j++) {
				$p = strpos($f[$j], $a[$i], $o);
					if ($p !== false)
						$r[] = [$p, $f[$j], $a[$i] ];
				}
			}
			else {
				$p = strpos($f, $a[$i], $o);
				if ($p !== false)
					$r[] = [$p, $f[$j], $a[$i] ];
			}
		}
		if (count($r) == 0)
			return 0;
		return $r;
	}
	
function imScale($res, $w, $h) {
	$sizeW = imagesx($res);
	$sizeH = imagesy($res);
	if ($sizeH == 0) return $res;
	$ratio = $sizeW/$sizeH;
	$wPrev = $w;
	$hPrev = $h;
	$xStart = 0;
	$yStart = 0;
	if ($ratio > 1) { 
		$h = $w / $ratio;
		if ($h != $hPrev)
			$yStart = ($hPrev - $h) / 2;
	}
	else if ($ratio < 1) { 
		$w = $h * $ratio;
		if ($w != $wPrev) 
			$xStart = ($wPrev - $w) / 2;
	}
	$im = imagecreatetruecolor($wPrev, $hPrev);
	$opacity = imagecolorallocatealpha($im, 0, 0, 0, 127);
	imagefill($im, 0, 0, $opacity);
	imagecopyresampled($im, $res, $xStart, $yStart, 0, 0, $w, $h, $sizeW, $sizeH);
	return $im;
}

function generateMiniImages($img, &$miniImg, $path, $q = 0) {	
	//echo 'MINIATURY!!';
	//print_r($img);
	for ($i = 0; $i < count($img); $i++) {
		if ( $img[$i] === 'content.json' || $img[$i] === 'intro.jpg')
			continue;
		$hasMini = false;
		for ($j = 0; $j < count($miniImg); $j++) {
			if (strpos($miniImg[$j], $img[$i]) !== false) {
				$hasMini = true;
				break;
			}			
		}
		if (!$hasMini && $img[$i] !== 'content.json' && $img[$i] !== 'intro.jpg') {
			//$hasAlpha = false;
			$imgnameArray = explode('.', $img[$i]);
			//print_r($imgnameArray);
			if ($imgnameArray[1] == 'png') {				
				$im = @imagecreatefrompng("{$path}/{$img[$i]}");
				//$hasAlpha = true;
			}
			else {
				$im = @imagecreatefromjpeg("{$path}/{$img[$i]}");
			}
			$im2 = imScale($im, 300, 200);
// 			$im2 = @imagescale($im, 300, 200);
// 			if ($hasAlpha) {
				@imageAlphaBlending($im2, true);
				@imageSaveAlpha($im2, true);
// 			}
			
			@imagepng($im2, "{$path}/{$imgnameArray[0]}_mini.png", $q);
			$miniImg[] = "{$imgnameArray[0]}_mini.png";
			@imagedestroy($im2);
			@imagedestroy($im);
		}
	}
}

function checkGalleryVersion() {
	if (file_exists("./galleries/galleries.content") && file_exists("./galleries/md5"))
		if (file_get_contents("./galleries/md5") === md5_file("./galleries/galleries.content"))
			return true;
	return false;
}

function generateContentFile($content) {
// 	if (checkGalleryVersion())
// 		return;
	$json = [];
	for ($i = 0; $i < count($content); $i++) {
		if ($content[$i] === 'content.json' || $content==='intro.jpg')
			continue;
		$json['photos'][$i]['photo'] = './galleries/' . $content[$i];
		$json['photos'][$i]['mini'] = './galleries/' . explode('.',$content[$i])[0] . '_mini.png';
		$json['photos'][$i]['alt'] = 'Zdjęcie naszych produktów';
	}
	file_put_contents('./galleries/content.json', '[' . json_encode($json) . ']');
} 

function getJSONFiles($folder) {
	if (!isset($folder)) $folder = './json';
	$out = [];
	$i = 0;
	foreach(scandir($folder) as $entry) {
		if ($entry[0] !== '.')
			$out[$i++] = $entry;
	}
	return $out;
}

function createMini($folder) {
	$d = dir($folder);
	$out = [];
	$name = [];
	while($entry = $d->read()) {
		if ($entry === 'content.json' || $entry === 'intro.jpg')
			continue;
		else if (strpos($entry, '.')) {
			if (strpos($entry, 'mini'))
				$out[count($out)] = $entry;
			else
				$name[count($name)] = $entry;
			
		}
// 		else
// 			$dirContent[$i++] = $entry;
	}
	//print_r($out);
	generateMiniImages($name, $out, $folder, 3);
	generateContentFile($name);
	return $out;
}

//funkcja ma za zadanie wczytywać dane ze wskazanego pliku json i wracać je do wykorzystania w kodzie
//$folder - lokaliazacja pliku json (bez nazwy pliku)
//$name - nazwa pliku json to wczytania
//$subArray - jeżeli chcemy wczytać konkretną gałąź pliku (może mieć różne zbiory) to podajemu tuitaj jego nazwę
//$id - id podgałęzi, którą chcemy wczytać; domyslne -1 wczytuje wszystkie gałęzie
function readEntries($folder, $name, $subArray = '', $id = -1) {
	$o;
	if ($subArray !== '')
		$o = json_decode(@file_get_contents($folder . '/' . $name), true)[0][$subArray];
	else
		$o = json_decode(@file_get_contents($folder . '/' . $name), true)[0];
	if ($id != -1 && $id < count($o))
		return $o[$id];
	return $o;
}

function save_tmp_file($dir, $n, $data, $mode = 'a') {	
	if(!file_exists($dir)) {
		mkdir($dir, 0755, true);
	}
	$f = fopen(__DIR__ . '/' . $dir . $n, $mode);
	fwrite($f, $data);
	fclose($f);
	
}

function saveFile($dir, $n, $count) {
	$fi = fopen(__DIR__ . '/' . $dir . $n . '.tmp_' . 0, 'r');
	$fo = fopen(__DIR__ . '/' . $dir . $n . 'b64', 'w');
	if (!$fi || !$fo) {
		return;
	}
	
	$sRead = fread($fi, filesize(__DIR__ . '/' . $dir . $n . '.tmp_' . 0));
	$sRead = str_replace(' ','+',explode(',', $sRead)[1]);
	fwrite($fo, $sRead);
	fclose($fi);
	unlink(__DIR__ . '/' . $dir . $n . '.tmp_' . 0);
	for ($i = 1; $i < $count; $i++) {
		$fi = fopen(__DIR__ . '/' . $dir . $n . '.tmp_' . $i, 'r');
		if (!$fi) break;
		$sRead = str_replace(' ','+',fread($fi, filesize(__DIR__ . '/' . $dir . $n . '.tmp_' . $i)));
		fwrite($fo, $sRead);
		fclose($fi);
		unlink(__DIR__ . '/' . $dir . $n . '.tmp_' . $i);
	}
	fclose($fo);
	$fi = fopen(__DIR__ . '/' . $dir . $n . 'b64', 'r');
	$fo = fopen(__DIR__ . '/' . $dir . $n, 'w');
	
	if (!$fi || !$fo) {
		return;
	}
	while (!feof($fi)) 
		fwrite($fo, base64_decode(fread($fi, 4096)));
	fclose($fi);
	fclose($fo);
	unlink(__DIR__ . '/' . $dir . $n . 'b64');
	$fInfoType = finfo_open(FILEINFO_MIME_TYPE);
	$mime = finfo_file($fInfoType, __DIR__ . '/' . $dir . $n);
	finfo_close($fInfoType);
	if (strpos($mime, 'text')) unlink(__DIR__ . '/' . $dir . $n);
}

function removeDir($path) {
	if (!is_dir($path)) {
		$tmp = explode('/', $path);
		$path = '';
		for($i = 0; $i < count($tmp) - 1; $i++) {
			$path .= $tmp[$i] . '/';
		}
	}
	$folder_files = @scandir($path);
	for ($z = 0; $z < count($folder_files); $z++) {
		
		if ($folder_files[$z] == '..' || $folder_files[$z] == '.') {
			$folder_files[$z] = '';
			continue;
		}
		if (is_dir($folder_files[$z])) removeDir($path);
		if ($folder_files[$z] != '')
			unlink($path . '/' . $folder_files[$z]);
	}
	rmdir($path);
}