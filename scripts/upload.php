<?php 
	@session_start();
	require_once "./utilityFunctions.php";
	
	if (isset($_POST['filePath'])) {		
		$tmp = explode('/', $_POST['filePath']);
		$dir = '';
		$name = $tmp[count($tmp) - 1];
		for($i = 0; $i < (count($tmp) - 1); $i++)
			$dir .= $tmp[$i] . '/';
		save_tmp_file($dir, $name . '.tmp_' . $_POST['secquence'], $_POST['data'], $_POST['mode']);
		if ( !isset($_SESSION['files'][$name]) ) {
			$_SESSION['files'][$name] = -1;
		}
		$_SESSION['files'][$name]++;

		echo $_SESSION['files'][$name];
		return;
	}
	else if (isset($_POST['transfer'])) {
		$tmp = explode('/', $_POST['transfer']);
		$dir = '';
		$name = $tmp[count($tmp) - 1];
		for($i = 0; $i < (count($tmp) - 1); $i++)
			$dir .= $tmp[$i] . '/';
		$_SESSION['files'][$name] = -1;
		unset($_SESSION['files'][$name]);		
		saveFile($dir, $name, $_POST['total']);
	}