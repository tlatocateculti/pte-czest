<?php
	$dirTmp = explode('/', __DIR__);
	$workDir = '';
	for ($i = 0; $i < count($dirTmp); $i++) {
		$workDir .= $dirTmp[$i] . '/';
		if ($dirTmp[$i] === 'scripts') break;
	}
	include($workDir . "dbVar.php");

	interface DataEnum {
		//pelna nazwa
		const EQUAL = 0;
		const GREATER = 1;
		const LOWER = 2;
		const NOT = 3;
		const GREATEREQUAL = 4;
		const LOWEREQUAL = 5;
		const LIKE = 6;
		const BETWEEN = 7;
		const IN = 8;
		const ORDERDESC = 9;
		const ORDERASC = 10;
		const GROUPBY = 11;
		const ISNULL = 12;
		const ISNOTNULL = 13;
		
		//krotka
		const DEQ = 0;
		const DHI = 1;
		const DLO = 2;
		const DNOT = 3;
		const DHIEQ = 4;
		const DLOEQ = 5;
		const DLIKE = 6;
		const DBET = 7;
		const DIN = 8;
		const DODESC = 9;
		const DOASC = 10;
		const DGBY = 11;
		
		//polaczenia w bazie
		const DOR = 0;
		const DAND = 1;
	}
	
	class Database implements DataEnum{
		private $db;
		private $dbtype;
		//opcjonalnie wypelniana tabela, w ktorej beda znajdować się nazwy WSZYSTKICH
		//tabel w bazie danych; zmienna ta potrzebna jest jedynie w przypadku uzycia funkcji 
		//wykonania skryptu SQL poprzez zapytanie!!
		private $dbNames;
		//zmienna przechowuje kolejne wyniki zapytan;
		//tablica wielowymiarowa; posiada klucz jako kolejne odpowiedzi 
		//
		private $queryResults;
		
		//tablica zawierajaca warunki zapytania
		private $conditionString;
		//aktualny wskaznik na edytowany warunek
		private $condIndex;
		
		private $transactionError;
		
		public function connect($dbuser, $dbpass, $dbname, $dbaddress = BASEADDRESS, $dbport = BASEPORT, $dbtype = BASETYPE, $dbcharset = "utf8") {
			try {
				$this->db = new PDO("{$dbtype}:host={$dbaddress};port={$dbport};dbname={$dbname};charset={$dbcharset}",$dbuser,$dbpass);
				$this->dbtype = $dbtype;
				$this->conditionString[0] = "";
				$this->condIndex = 0;
				return true;
			}
			catch (PDOException $e) {
				echo 'Połączenie nie powiodło się: ' . $e->getMessage();
				return false;
			}
			
		}
		
		public function disconnect() {
			unset($db);
			unset($dbtype);
		}
		
		public function flushResults() {
			unset($this->queryResults);
		}
		
		public function getResults($i) {
			if (!isset($this->queryResults))
				return -1;
			if ($i < count($this->queryResults))
				return $this->queryResults[$i];
			else 
				return 0;
		}
		
		public function beginTransaction() {
// 			$qStr = "";
// 			if ($this->dbtype === "mysql") {
// 				$qStr = "START TRANSACTION;";
// 			}
			$this->db->beginTransaction();
			//$this->db->exec($qStr);
		}
		
		public function commitTransaction() {
// 			$qStr = "";
// 			if ($this->dbtype === "mysql") {
// 				$qStr = "COMMIT;";
// 			}
			$this->db->commit();
// 			$this->db->exec($qStr);
		}
		
		public function rollbackTransaction() {
// 			$qStr = "";
// 			if ($this->dbtype === "mysql") {
// 				$qStr = "ROLBACK;";
// 			}
			$this->db->rollback();
// 			$this->db->exec($qStr);
		}
		
		//funckcja zwraca ilosc rekordow, ktore zwroci zapytanie. 
		//$tables - zaleznie od uzycia; w przypadku SELECT identycznie jak w przypadku zapytania z funkcji queryTable
		//$conditions - kryteria wyniku; mozna uzyc funkcji budowy warunkow
		//WAZNE - dorobic poprawne dzialanie funkcji np. dla SHOW CREATE TABLE		
		public function countQuery($tables, $conditions = "") {
			$qStr = "";
			$keys = array_keys($tables);
			if ($this->dbtype === "mysql") {
				$qStr .= "SELECT";
				$databases = "";
				for ($i = 0; $i < count($tables); $i++) {
					for ($j = 0; $j < count($tables[$keys[$i]]); $j++) {
						if ($tables[$keys[$i]][$j] == "*")
							$qStr .= " `{$keys[$i]}`.{$tables[$keys[$i]][$j]},";
						else
							$qStr .= " `{$keys[$i]}`.`{$tables[$keys[$i]][$j]}`,";
					}
					$databases .= " `{$keys[$i]}`,";					
				}
				$qStr = $qStr = substr($qStr, 0, -1) . " FROM " . substr($databases, 0, -1);
				if ($conditions != "") 
					$qStr .= " WHERE {$conditions}";
				$qStr .= ";";
			}
			try {
// 				echo $this->db->query($qStr)->rowCount();
				if ($this->db->query($qStr))
					return $this->db->query($qStr)->rowCount();
				else
					return 0;
			}
			catch (PDOException $e) {
				echo 'Zapytanie nie powiodło się: ' . $e->getMessage();
				return -1;
			}
// 			echo $qStr;
		}
		
		//najprostsza funkcja pobierajaca dane ze wskazanych pol danej tabeli
		//$tables - zmienna tablicowa, gdzie nazwa indeksu jest nazwa tabeli, a kolejne podtabele nazwami wybieranych pol
		//$conditions - odpowiednio zbudowane warunki (odpowiednia funkcja) lub wlasne ryzyko:)
		public function queryTable($tables, $conditions = "", $limit = 100, $start = 0) {
			if (!is_array($tables)) 
				return "-200, Nie podałeś tablicy wartości!";
			$qStr = "";
			$keys = array_keys($tables);
			//$fieldName;
			//$fieldNamePtr = 0;
			if ($this->dbtype === "mysql") {
				$qStr .= "SELECT";
				$databases = "";
				for ($i = 0; $i < count($tables); $i++) {
					for ($j = 0; $j < count($tables[$keys[$i]]); $j++) {
						//$fieldName[$fieldNamePtr] = "{$keys[$i]}_{$tables[$keys[$i]][$j]}";
						if ($tables[$keys[$i]][$j] == "*")
							$qStr .= " `{$keys[$i]}`.{$tables[$keys[$i]][$j]},";
						else
							$qStr .= " `{$keys[$i]}`.`{$tables[$keys[$i]][$j]}`,";
						//$fieldNamePtr++;
					}
					$databases .= " `{$keys[$i]}`,";					
				}				
				$qStr = substr($qStr, 0, -1) . " FROM " . substr($databases, 0, -1);				
				if ($conditions != "") 
					$qStr .= " WHERE {$conditions}";
				if ($limit > -1) {
					$qStr .= " LIMIT {$limit}";				
					if ($start != 0)
						$qStr .= " OFFSET {$start}";
				}
				$qStr .= ";";
			}
			$resId = 0;
			
			if(isset($this->queryResults))
				$resId = count($this->queryResults);
			$resCount = 0;
			$pdoEquals = $this->db->query($qStr);
			try {
				if (is_object($pdoEquals))
					foreach($pdoEquals as $row) {
					//SELECT `sites`.*, `users`.`name`, `users`.`email` FROM `sites`, `users` LIMIT 100 OFFSET 100;
						$rowKeys = array_keys($row);
						for ($i = 0; $i < count($rowKeys); $i+=2) 
							$this->queryResults[$resId][$rowKeys[$i]][$resCount] = $row[$rowKeys[$i]];
						$resCount++;
					}
			}
			catch (PDOException $e) {
				echo 'Zapytanie nie powiodło się: ' . $e->getMessage();
				return -1;
			}
// 			echo $qStr;
		}
		
		public function insert($table, $values) {
			if (!is_array($values)) 
				return "-200, Nie podałeś tablicy wartości!";
			$keys = array_keys($values);
			$qValues = "";
			$qStr = "";			
			if ($this->dbtype === "mysql") {
				$qStr = "INSERT INTO `{$table}` (";
				if (count($keys) > 0) {
					$qStr .= "`" . $keys[0] . "`";
					$qValues .= "'" . $values[$keys[0]] . "'";
				}
				for ($i = 1; $i < count($keys); $i++) {
					$qStr .= ",`" . $keys[$i] . "`";
					$qValues .= ",'" . $values[$keys[$i]] . "'";
				}
				$qStr .= ") VALUES ({$qValues});";
			}
			try {
				$this->db->exec($qStr);
// 				echo '<pre>' . $qStr . '</pre>';
			}
			catch (PDOException $e) {
				echo 'Zapytanie insert wywołało błąd: ' . $e->getMessage();
			}
		}
		
		public function update($table, $values, $criteria = "") {
			if (!is_array($values)) 
				return "-200, Nie podałeś tablicy wartości!";
// 			if (!isset($criteria)) 
// 				return "-210, Nie podałeś kryteriów usuwania!";
// 			if (empty($criteria)) 
// 				return "-211, Podałeś jako kryterium pustą wartość!";
			$keys = array_keys($values);
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr = "UPDATE `{$table}` SET ";
				if (count($keys) > 0) {
					$qStr .= "`{$keys[0]}`='{$values[$keys[0]]}'";
				}
				for ($i = 1; $i < count($keys); $i++) {
					$qStr .= ",`{$keys[$i]}`='{$values[$keys[$i]]}'";
				}
				if ($criteria != "")
					$qStr .= " WHERE {$criteria};";
			}
			//echo $qStr;
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
		}
		
		public function delete($table, $criteria) {
			if (!isset($criteria)) 
				return "-210, Nie podałeś kryteriów usuwania!";
			if (empty($criteria)) 
				return "-211, Podałeś jako kryterium pustą wartość!";
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr = "DELETE FROM `{$table}` WHERE {$criteria};";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
// 			echo $qStr;
		}
		
		//ZOPTYMALIZOWAĆ; 
		//funkcja wykonuje zapisaną (składowaną procedurę PL/SQL)
		// $name - nazwa wykonywanej procedury
		// $params - podawany jako lista; przechowuje podawane argumenty do procedury
		public function callProcedure($name, $params) {
			if (!is_array($params)) return "-211, parametry nie są w postaci tablicy!";
			if (empty($name)) 
				return "-211, Podałeś jako nazwę procedury pustą wartość!";
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr = "CALL {$name}(";
				for ($i = 0; $i < count($params); $i++) 
					$qStr .= "'" . $params[$i] . "',";
				if (count($params) > 1)
					$qStr = substr($qStr, 0, strlen($qStr) - 1);
				$qStr .= ");";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
// 			echo $qStr;
		}
		
		//$fields - zmienna tablicowa wedle wzorca $fields[{nazwa_pola}] => $field[0] zawiera typ, opcjonalne $field[1] zawiera dlugosc (bajty)
		//opcjonalne $field[2] zawiera dyrektywe dodatkowa (np. NOT NULL, AUTO_INCREMENT itp.);w tym wypadku $field[1] musi miec wartos 0!
		//$primaryField - nazwa pola - klucza glownego
		//$forceCreate - wartosc true wymusza utworzenie nawet, gdy taka tabela juz istnieje (kasuje poprzednia!)
		public function createTable($table, $fields, $primaryField = "", $forceCreate = false) {
			if (!is_array($fields)) 
				return "-200, Nie podałeś tablicy wartości!";
			$keys = array_keys($fields);
			//mozliwe, ze to sie poszerzy o sprawdzenie wszystkich wartosci w tablicy
			if (!is_array($fields[$keys[0]])) 
				return "-201, Brak tablicy typów/długość pól!";
			$qStr = "";
			if ($this->dbtype === "mysql") {
				if ($forceCreate)
					$qStr .= "DROP TABLE IF EXISTS `{$table}`; ";
				$qStr .= "CREATE TABLE IF NOT EXISTS `{$table}` (";
				if (count($keys) > 0) {
					$qStr .= "{$keys[0]} {$fields[$keys[0]][0]}";
					if (isset($fields[$keys[0]][1]))
						if ($fields[$keys[0]][1] != 0)
							$qStr .= "({$fields[$keys[0]][1]})";
					if (isset($fields[$keys[0]][2]))
						$qStr .= " {$fields[$keys[0]][2]}";
				}
				for ($i = 1; $i < count($keys); $i++) {
					$qStr .= ",{$keys[$i]} {$fields[$keys[$i]][0]}";
					if (isset($fields[$keys[$i]][1]))
						if ($fields[$keys[$i]][1] != 0)
							$qStr .= "({$fields[$keys[$i]][1]})";
					if (isset($fields[$keys[$i]][2]))
						$qStr .= " {$fields[$keys[$i]][2]}";
				}
				if ($primaryField != "")
					$qStr .= ", PRIMARY KEY({$primaryField}));";
				else
					$qStr .= ");";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
			//echo $qStr;
		}
		
		public function dropTable($table) {
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr .= "DROP TABLE IF EXIST `{$table}`;";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
		}
		
		public function renameTable($table, $name) {
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qstr .= "RENAME TABLE `{$table}` TO '{$name}';";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
		}
		
		//$newcolumn - tablica zawierajaca kolejno:
		//[0] - nazwe nowej kolumny
		//[1] - typ nowej kolumny
		//[2] - (opcjonalny) pozwala okreslic dlugosc zmiennej (np. na 8 bajtow)
		//[3] - (opcjonlany) pozwala dodac dodatkowe parametry dla pola, np. AUTO_INCREMENT badz ZEROFILL
		//$pos - mozna wpisac tylko FIRST badz AFTER
		//$posAfter - jezeli w $pos wpisalismy AFTER to w tym parametrze podajemy nazwe pola, po ktorym chcemy dolaczyc nowe pole
		public function addColumn($table, $newColumn, $pos = "", $posAfter = "") {
			if (!is_array($newColumn)) 
				return "-200, Nie podałeś tablicy wartości!";
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr .= "ALTER TABLE `{$table}` ADD COLUMN {$newColumn[0]} {$newColumn[1]}";
				if (isset($newColumn[2]))
					$qStr .= "({$newColumn[2]})";
				if (isset($newColumn[3]))
					$qStr .= " $newColumn[3]";
				//tutaj sie zastanowic czy tego nie zoptymalizowac bardziej!
				if ($pos != "") {
					$qStr .= " $pos";
					if ($pos == "AFTER" && $posAfter != "") 
						$qStr .= " $posAfter";
				}
				$qStr .= ";";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
			//echo $qStr;
		}
		
		public function changeColumn($table, $oldColumnName, $newColumn, $pos = "", $posAfter = "") {
			if (!isset($oldColumnName))
				return "-212, Nie podałeś nazwy zmienianej kolumny!";
			if (!is_array($newColumn)) 
				return "-200, Nie podałeś tablicy wartości!";
			$qStr = "";
			if  ($this->dbtype === "mysql") {
				$qStr .= "ALTER TABLE `{$table}` CHANGE COLUMN `{$oldColumnName}` {$newColumn[0]} {$newColumn[1]}";
				if (isset($newColumn[2]))
					$qStr .= "({$newColumn[2]})";
				if (isset($newColumn[3]))
					$qStr .= " $newColumn[3]";
				//tutaj sie zastanowic czy tego nie zoptymalizowac bardziej!
				if ($pos != "") {
					$qStr .= " $pos";
					if ($pos == "AFTER" && $posAfter != "") 
						$qStr .= " $posAfter";
				}
				$qStr .= ";";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
		}
		
		public function modifyColumnType($table, $column, $newType, $pos = "", $posAfter = "") {
			if (!isset($column))
				return "-212, Nie podałeś nazwy usuwanej kolumny!";
			if (!is_array($newType)) 
				return "-200, Nie podałeś tablicy wartości!";
			$qStr = "";
			if  ($this->dbtype === "mysql") {
				$qStr .= "ALTER TABLE `{$table}` MODIFY COLUMN `{$column}` {$newType[0]}";
				if (isset($newType[1]))
					$qStr .= "({$newType[1]})";
				if (isset($newType[2]))
					$qStr .= " $newType[2]";
				//tutaj sie zastanowic czy tego nie zoptymalizowac bardziej!
				if ($pos != "") {
					$qStr .= " $pos";
					if ($pos == "AFTER" && $posAfter != "") 
						$qStr .= " $posAfter";
				}
				$qStr .= ";";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
			//echo $qStr;
		}
		
		//$primaryTable - tabela, gdzie pod wartosc [0] podlacza sie nazwe tabeli, pod wartosc[1] nazwe pola
		//$secondaryTable - analogicznie do poprzedniej
		//$foreignType - mozna wymusic dodatkowo typ indeksu (UNIQUE, FULLTEXT, SPATIAL)
		//GDY KLUCZ JUZ JEST W BAZIE - po prostu nic sie nie wykona
		public function addForeignKey($primaryTable, $secondaryTable, $foreignType = "") {
			if (!is_array($primaryTable)) 
				return "-200, Nie podałeś tablicy wartości pierwszej tabeli!";
			if (!is_array($secondaryTable)) 
				return "-200, Nie podałeś tablicy wartości drugiej tabeli!";
			$qStr = "";
			if ($this->dbtype === "mysql") {
			//to przydlugie polecenie sprawdza, czy istnieje wskazany indeks; jezeli  nie - doda odpowiedni
				if (!$this->countQueryModal(array($primaryTable[0] => array($primaryTable[1])), 
					"SHOW INDEX", "`Column_name` = '{$primaryTable[1]}'")) {
					$qStr .= "CREATE";
					if ($foreignType != "" )
						$qStr .= " {$foreignType}";
					$qStr .= " INDEX i_{$primaryTable[1]} ON `{$primaryTable[0]}` (`{$primaryTable[1]}`);";
				}				
				$qStr .= " ALTER TABLE `{$primaryTable[0]}` ADD CONSTRAINT 
						fk_{$primaryTable[1]}_{$secondaryTable[1]} FOREIGN KEY ({$primaryTable[1]}) REFERENCES {$secondaryTable[0]}({$secondaryTable[1]});";
				
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
			//echo $qStr;
		}
		
		//$fieldName - nazwa pola, do ktorego przypisany jest klucz obcy
		public function removeForeignKey($table, $fieldName) {
			if (!isset($column))
				return "-212, Nie podałeś nazwy usuwanego klucza obcego!";
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr .= "ALTER TABLE `{$table}` DROP FOREIGN KEY `fk_{$fieldName}`;";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
		}
		
		public function removeColumn($table, $column) {
			if (!isset($column))
				return "-212, Nie podałeś nazwy usuwanej kolumny!";
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr .= "ALTER TABLE `{$table}` DROP COLUMN `{$column}`;";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
		}
		
		
		public function createDatabase($name) {
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr .= "CREATE DATABASE `{$name}`;";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
		}
		
		public function switchDatabase($name) {
			$qStr = "";
			if ($this->dbtype === "mysql") {
				$qStr .= "USE `{$name}`;";
			}
			//mozna jeszcze sprawdzic czy jest polecenie
			$this->db->exec($qStr);
		}
		
		//funckcja zwraca ilosc rekordow, ktore zwroci zapytanie. 
		//$tables - zaleznie od uzycia; w przypadku SELECT identycznie jak w przypadku zapytania z funkcji queryTable
		//w przypadku np. odpytania o indeksy - przyjmuje jedynie nazwy tabel (w kolejnych czesciach tabeli) 
		//$queryType - domyslnie "SELECT", mozna podac "SHOW INDEX", "SHOW KEYS", "SHOW CREATE TABLE" itp.s		
		//WAZNE - dorobic poprawne dzialanie funkcji np. dla SHOW CREATE TABLE
		private function countQueryModal($tables, $queryType = "SELECT", $conditions = "") {
			$qStr = "";
			$keys = array_keys($tables);
			if ($this->dbtype === "mysql") {
				$qStr .= $queryType;
				$databases = "";
				for ($i = 0; $i < count($tables); $i++) {
					if ($queryType == "SELECT") {
						for ($j = 0; $j < count($tables[$keys[$i]]); $j++) {
							if ($tables[$keys[$i]][$j] == "*")
								$qStr .= " `{$keys[$i]}`.{$tables[$keys[$i]][$j]},";
							else
								$qStr .= " `{$keys[$i]}`.`{$tables[$keys[$i]][$j]}`,";
						}
					}
					$databases .= " `{$keys[$i]}`,";					
				}
				if ($queryType == "SELECT")
					$qStr = substr($qStr, 0, -1);
				$qStr .= " FROM " . substr($databases, 0, -1);
				if ($conditions != "") 
					$qStr .= " WHERE {$conditions}";
				$qStr .= ";";
			}
			try {
				return $this->db->query($qStr)->rowCount();
			}
			catch (PDOException $e) {
				echo 'Zapytanie nie powiodło się: ' . $e->getMessage();
				return -1;
			}
		}
		
		//DO ZROBIENIA: dodac mozliwosc edycji wczesniej 'zamknietego' warunku (byc moze jako osobna funkcja)
		//Funkcja tworzy ciag warunkow opary o relacje OR/AND. 
		//$dest - tablica, gdzie [0] to nazwa tablicy, [1] to nazwa kolumny
		//$conds - tablica warunkow; moze byc pusta tablica w przypadku ORDER BY, w pozostałych przypadkach 
		//minimalnie musi byc jeden warunek, dwa (BETWEEN) badz wieksza ilosc (IN)
		//$compareAction - jako wartosc podaje sie numer operacji 
		//$relationToOthers - jezeli mamy juz dodane wczesniejsze warunki to tutaj podajemy relacje do nich (OR lub AND)
		//$otherAsOne - jezeli true laczy dotychczasowe warunki w jeden, ktory musi zostac spelniony
		//nalezy dorobic idiotoodporna funkcje, ktora nie pozwoli na umieszczenie GROUP BY przed warunkami, a ORDER BY 
		//zawsze przemiesci na ostatnie miejsce (i zabroni dodawac wielokrotnie)
		public function buildConditionQuery($dest, $conds, $compareAction, $relationToOthers = -1, $otherAsOne = false) {
			if (!is_array($dest))
				return "-200, Pierwszy parametr nie jest tablicą!";
			if ($relationToOthers != -1 && $this->conditionString[$this->condIndex] != "") {
				if ($otherAsOne) {
					$this->conditionString[$this->condIndex] = " ({$this->conditionString[$this->condIndex]})";
					$this->conditionString[++$this->condIndex] = "";
				}
				switch ($relationToOthers) {
					case 0: $this->conditionString[$this->condIndex] .= " OR"; break;
					case 1: $this->conditionString[$this->condIndex] .= " AND"; break;
				}
			}
			if ($compareAction < 9 || $compareAction > 11)
				$this->conditionString[$this->condIndex] .= " (`{$dest[0]}`.`{$dest[1]}`";
			else if ($this->conditionString[$this->condIndex] == '')
				$this->conditionString[$this->condIndex] .= " 1";
			switch ($compareAction) {
				case 0: $this->conditionString[$this->condIndex] .= " = '{$conds[0]}'"; break;
				case 1: $this->conditionString[$this->condIndex] .= " > '{$conds[0]}'"; break;
				case 2: $this->conditionString[$this->condIndex] .= " < '{$conds[0]}'"; break;
				case 3: $this->conditionString[$this->condIndex] .= " <> '{$conds[0]}'"; break;
				case 4: $this->conditionString[$this->condIndex] .= " >= '{$conds[0]}'"; break;
				case 5: $this->conditionString[$this->condIndex] .= " <= '{$conds[0]}'"; break;
				case 6: $this->conditionString[$this->condIndex] .= " LIKE '{$conds[0]}'"; break;
				case 7: $this->conditionString[$this->condIndex] .= " BETWEEN '{$conds[0]}' AND '{$conds[1]}'"; break;
				case 8: $this->conditionString[$this->condIndex] .= " IN ("; 
					if (count($conds) > 0)
						$this->conditionString[$this->condIndex] .= "'{$conds[0]}'";
					for ($i = 1; $i < count($conds); $i++) {
						$this->conditionString[$this->condIndex] .= ",'{$conds[$i]}'";
					}
					$this->conditionString[$this->condIndex] .= ")"; break;
				case 9: $this->conditionString[$this->condIndex] .= " ORDER BY `{$dest[0]}`.`{$dest[1]}` DESC"; break;
				case 10: $this->conditionString[$this->condIndex] .= " ORDER BY `{$dest[0]}`.`{$dest[1]}` ASC"; break;
				case 11: $this->conditionString[$this->condIndex] .= " GROUP BY `{$dest[0]}`.`{$dest[1]}`"; break;
				case 12: $this->conditionString[$this->condIndex] .= " IS NULL"; break;
				case 13: $this->conditionString[$this->condIndex] .= " IS NOT NULL"; break;
			}
			if ($compareAction < 9 || $compareAction > 11)
				$this->conditionString[$this->condIndex] .= ")";
		}
		
		public function getConditions() {
			$qStr = "";
			for ($i = 0; $i < $this->condIndex + 1; $i++) {
				$qStr .= $this->conditionString[$i];
			}
			return $qStr;
		}
		
		public function flushConditions() {
			unset($this->conditionString);
			$this->conditionString[0] = "";
			$this->condIndex = 0;
		}
		
		//funkcja wykonuje caly, podany skrypt na bazie danych
		//w trosce o bezpieczenstwo skrypt wykona się tylko w przypadku ustawienia nazw wszystkich baz
		//w bazie danych (zmienna prywtna $dbNames)
		//UWAGA! trzeba by pomyslec o dodatkowych zabezpieczeniach, jak np. dodatkowa weryfikacja
		//do bazy danych i, najwazniejsze, zliczac ile baz danych skrypt obejmuje (na razie wszystko jest na szybko)
		public function executeScript($str) {
			$p = 0;
			//print_r($this->dbNames);
			foreach($this->dbNames as $db) {
				$find = strpos($str, $db, $p);
				//echo $find . "{$str} {$db} {$p}<br/>";
				if ($find === false) {
					echo "<p>Podałeś niewłaściwe dane, odmawiam wykonania!!</p>";
					return;
				}
				$p = $find;
			}
			if ($p > -1) {
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				try {
					$this->db->exec($str);
					//print_r($this->db->errorInfo());
				}
				catch(PDOException $e) {
					echo 'Błąd bazy danych ' . $e->getMessage();
				}
				//echo 'WYKONANO!! ' . $str;
			}
		}
		
		public function addDbName($s) {
			$this->dbNames[count($this->dbNames)] = $s;
		}
	}
	
