<?php
	require_once "database.php";

	function getContentSites($bs, $sites) {
		$output = '';
		for ($i = 0; $i < count($sites); $i++) {
			$bs->flushConditions();
			$bs->flushResults();
			$tmp = explode('/', $sites[$i]);
			if ($tmp[0] === 'news') {				
				$bs->buildConditionQuery(array('news', 'news_id'), array($tmp[1]), DataEnum::EQUAL);
				$bs->buildConditionQuery(array('news', 'show'), array(1), DataEnum::EQUAL, DataEnum::DAND);
				$bs->queryTable(array('news' => array('topic', 'summary')), $bs->getConditions(), 1);				
			}
			else if ($tmp[0] === 'courses') {
				$bs->buildConditionQuery(array('courses', 'course_id'), array($tmp[1]), DataEnum::EQUAL);
				$bs->buildConditionQuery(array('courses', 'active'), array(1), DataEnum::EQUAL, DataEnum::DAND);
				$bs->queryTable(array('courses' => array('topic', 'additional_info')), $bs->getConditions(), 1);		
			}
// 			else if($tmp[0] === 'sites') {
// 				$bs->buildConditionQuery(array('sites', 'site_id'), array($tmp[1]), DataEnum::EQUAL);
// 				$bs->buildConditionQuery(array('sites', 'active'), array(1), DataEnum::EQUAL, DataEnum::DAND);
// 				$bs->queryTable(array('sites' => array('text')), $this->sql->getConditions(), 1);		
// 			}
			if ($bs->getResults(0) > 0) {
				$output .= "<article class=\"search_pos\" onclick=\"openSearch('{$sites[$i]}');\">
						<h4>" . $bs->getResults(0)['topic'][0] . "</h4><p>" .
				(($tmp[0] === 'news') ? $bs->getResults(0)['summary'][0] : substr($bs->getResults(0)['additional_info'][0], 0, 100)) . ' (...)'
						. "</p></article>";
			}
		}
		return $output;
	}
	
	//najlepiej byłoby to, w celu optymalizacji, przebudować na JSON;
	//następnie w JavaScript tworzony byłby wynik, który nie wymagałby interwencji w PHP (szybsze) 
	function getArticle($site) {
		require_once "../scripts/database.php";
		require_once "../config.php";
		require_once "../scripts/utilityFunctions.php";
		
		$bs = new Database();
		$bs->connect(decodePhrase(BASE_USER), decodePhrase(BASE_PASS), BASE_NAME);
		$tmp = explode('/', $site);
		if ($tmp[0] === 'news') {				
			$bs->buildConditionQuery(array('news', 'news_id'), array($tmp[1]), DataEnum::EQUAL);
			$bs->queryTable(array('news' => array('*')), $bs->getConditions(), 1);	
			echo "<article>";
			echo '<h4>' . $bs->getResults(0)['topic'][0] . '</h4>';
			echo '<p>' . $bs->getResults(0)['full_text'][0] . '</p>';
			echo '<p class="signatureNews">Dodane przez: alterEgo, dnia <span>' . $bs->getResults(0)['dateAdd'][0] . '</span></p>';
	// 		echo '<p><span>' . $bs->getResults(0)['dateAdd'][0] . '</span><span id="autor">autor: admin</span></p>';
			echo '</article>';
			
		}
		else if ($tmp[0] === 'courses') {
			$bs->buildConditionQuery(array('courses', 'course_id'), array($tmp[1]), DataEnum::EQUAL);
			$bs->queryTable(array('courses' => array('*')), $bs->getConditions(), 1);	
			echo "<article class=\"courseArt\">";
				echo '<div class="courseHeader"';
// 				if (!$open) 
// 					echo ' onclick="selectArticle(this);';
				echo '"><h3>' . $bs->getResults(0)['topic'][0] . '</h3>';
// 				if (!$open)
// 					echo '<div class="stateButton"><i class="fa fa-chevron-down"></i></div>';
				echo '</div>';
				echo '<div';
// 				if ($open) 
					echo ' style="display: block;"';
// 				else 
// 					echo ' class="details"';
				echo '>';
					if ($bs->getResults(0)['course_startdate'][0] !== '0000-00-00')
						echo '
							<legend><b>Termin kursu</b></legend>
							<p>' . $bs->getResults(0)['course_startdate'][0] . '</p>';
					if ($bs->getResults(0)['objectives'][0] !== '')
						echo '
							<legend><b>Cel kursu</b></legend>';
							$obj = explode(';',  $bs->getResults(0)['objectives'][0]);
							for ($j = 0; $j < count($obj); $j++)
								echo '<p>' . $obj[$j] . '</p>';
						//echo '<p>Podsumowując...</p>';
	// 				echo '';
					echo '
						<legend><b>Informacje o kursie</b></legend>
						<p>' . $bs->getResults(0)['additional_info'][0] . '</p>
					';
					if ($bs->getResults(0)['prices'][0] !== '')
						echo '
							<legend><b>Cena kursu</b></legend>';
							$obj = explode(';',  $bs->getResults(0)['prices'][0]);
							for ($j = 0; $j < count($obj); $j++)
								echo '<p>' . $obj[$j] . '</p>';
	// 				echo '';
					echo "<div class=\"signUpButton\" onclick=\"openLink('m', 'contact', '/cc_" . $bs->getResults(0)['course_id'][0] . "');\">  Zapisz się już teraz! <i class=\"fa fa-hand-o-up\" aria-hidden=\"true\"></i>
	</div>";
			echo 	'</div>
			</article>';
		}
		$bs->flushConditions();
		$bs->flushResults();
	}
	
	
	if (isset($_POST['addr']))
		getArticle($_POST['addr']);