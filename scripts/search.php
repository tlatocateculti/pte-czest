<?php

require_once "database.php";
require_once "searchContent.php";
// require_once ("./files/php/class.dispalySearch.php");

/**
 * Klasa ma za zadanie przeszukiwac zapewnic obsluge indeksowania dla strony.
 * Dzieki niej mozemy dodac konkretne strony do indeksu lub etykiet,
 * skasowac je, a takze jest funkcja przeszukujaca indeks
 * @author Tezcatlipoca
 * @version 1.0
 *
 */
class IndexLib
{
	/**
	 * Zmienna przechowuje odnosnik (resource) z polaczeniem do serwera bazy danych lub wartosc bool (jezeli polaczenie nie zostalo zrealizowane)
	 * @var mixed
	 */
	private $sql;
	/**
	 * Zmienna przechowuje stan polaczenia z baza danych 
	 * @var bool
	 */
	private $base;
	/**
	 * Zmienna przechowuje polecenie do bazy danych
	 * @var string
	 */
	private $query;
	/**
	 * Zmienna przechowuje wartosc resource lub bool dla zapytania
	 * @var mixed
	 */
	private $execute_info;
	/**
	 * Zmienna przechowuje ile wynikow ma byc wyswietlanych na strone
	 * @var int
	 */
	private $results_on_page;
	/**
	 * Zmienna przechowuje obiekt klasy
	 * @var DisplaySearchResults
	 */
	private $display;
	/**
	 * Zmienna przechowuje obiekt klasy
	 * @var log
	 */
	private $log;
	
	/**
	 * Zeminna przechowuje pasek nawigacji po wynikach   
	 * @var string
	 */
	public $navigationString;
	
// 	/**
// 	 * Zmiennan zawiera tablice zwroconych wynikow
// 	 * @var array
// 	 */
// 	public $contentTable;
	
	/**
	 * Konstruktor klasy IndexLib
	 * @param int bs uchwyt do połączeniz bazą danych (na bazie PDO)
	 */
	function __construct($bs)
	{
		if (!$bs) return;
		$this->sql = $bs;
// 		$this->display = new DisplaySearchResults();
	}
	
	/**
	 * Destruktor klasy IndexLib
	 */
	function __destruct()
	{
		unset($this->sql);
		unset($this->base);
		unset($this->query);
		unset($this->execute_info);
// 		unset($this->result_by_page);
		unset($this->connect);
// 		unset($this->display);
		unset($this->navigationString);
	}
	
	/**
	 * Funkcja dodaje do bazy słowa-klucze dla wskazanego adresu oraz opcjonalnie etykiety
	 * @param int $mode jesli 0 - dodawanie do indexu i etykiet; 1 - dodawanie do samych etytkiet; 2 - dodawanie do samego indeksu (ewentualnie etykiety zdefiniowane)
	 * @param string $address zawiera adres strony, do ktorej maja odwolywac sie klucze lub etykiety 
	 * @param string $string1 tryb 0 - zawiera tresc do indeksowania stron; tryb 1 - zawiera etykiety dla danej strony
	 * @param string $string2 zmienna uzywana tylko w trybie 0 i 2; zawiera etykiety dla danej strony
	 * @return mixed zwraca blad lub TRUE w przypadku powodzenia
	 */
	public function addPhrase($mode = 0, $address = "", $string1 = "", $string2 = "")
	{
		
		if ((@strcmp($string1, "") == 0 && @strcmp($string2, "") == 0) || @strcmp($address, "") == 0) return "Nie ma nic do dodania";		
		if ($mode == 0 || $mode == 2)
		{
			$index = $this->changePhrase($string1, 1);		
			$index = @implode('||', $this->filtrKeyword($index));
			$string1 = $this->changePhrase($string1);
			$index_list = @implode('||', $this->filtrKeyword($string1)). '||';
			//$index_list = /*"||".*/ $index_list.'||';		
		}
		if ($mode == 1) $string2 = $string1;
		$string2 = $this->changePhrase($string2, 1);
		$string2 = @implode('||', $this->filtrKeyword($string2, 1));
		if ($mode == 0) 
		{		
			//byc moze tutaj trzeba będzie dołączyć $string2!!
			if ($string2 !== '') $index .= '||' . $string2;
			$this->insertTagsToTable($index, $address);
			$this->insertToIndex($index_list, $address);
		}
		else if ($mode == 1) $this->insertTagsToTable($string2, $address);
		else if ($mode == 2) 
		{
			$this->insertTagsToTable($string2, $address);
			$this->insertToIndex($index_list, $address);
		}	
		return 	true;
	}	
	
	/**
	 * Funkcja kasuje z bazy odpowiednie rekordy zgodnie z wybranym trybem
	 * @param int $mode jesli 0 - kasowane jest slowo z indeksu; 1 - kasowana jest strona; 2 - kasowana jest etykieta; 3 - kasowana jest strona + etykiety
	 * @param string $string1 zmienna przyjmujaca odpowiednia fraze(0 i 2 tryb) lub adres strony (tryb 1 i 3)
	 * @param string $string2 zmienna wykorzystywana w trybie drugim; zawiera adres strony
	 * @return mixed zwraca blad lub TRUE w przypadku powodzenia 
	 */
	public function delete($mode = 0, $string1 = "", $string2 = "")
	{
		if ($mode == 0)
		{
			$string1 = $this->changePhrase($string1);
			$return_value = $this->deleteFromIndex($string1);
		}
		else if ($mode == 1) $return_value = $this->deletePageFromIndex($string1);
		else if ($mode == 2) $return_value = $this->deleteTagFromTags($string1, $string2);
		else if ($mode == 3)
		{
			$return_value = $this->deletePageFromIndex($string1);
			$return_value = $this->deletePageFromTags($string1);
		}	
		return $return_value;
	}
	
	/**
	 * Funkcja zwraca etykiety lub strony, ktore sa reprezentowane przez liste slow-indekstow lub etykiet
	 * @param string $phrase lista slow-kluczy lub w przypadku mode = 1 adres strony
	 * @param int $mode tryb zwrocenia wynikow: 0 - zwraca adresy stron z indeksu, 1 - zwraca etykiety, 2 - zwraca adresy z etykiet
	 * @param int $page numer strony do wyswietlenia (numeracja od 0)
	 * @param int $register 0 - nie zapisuje czasu operacji w bazie; 1 - zapisuje czas operacji w bazie
	 * @return string zwraca kod z wynikami do umieszczenia na stronie
	 */
	public function returnContent($phrase, $mode = 0, $page = 0, $register = 0)
	{
		$output;
		if ($mode == 3)
		{
			//lista stron!!
			$output = $phrase;
			//strona tego nie posiada!!
			//$output = $this->display->displayPage($this->contentTable, $this->results_on_page, $page);
			//$this->navigationString = $this->display->createNavigation($phrase, $this->contentTable, $this->results_on_page, $page);
		}
		else if ($mode == 0) 
		{
			//$this->contentTable = $this->searchPhrase($phrase, $register);
			$output = getContentSites($this->sql, $this->searchPhrase($phrase, $register));
			
// 			$output = $this->display->displayPage($this->contentTable, $this->results_on_page, $page);
// 			$this->navigationString = $this->display->createNavigation($phrase, $this->contentTable, $this->results_on_page, $page);
		}
		else if ($mode == 1)
		{
			$tags = $this->getTags($phrase, $register);		
// 			$output = $this->display->displayTag($tags);
		}
		else if ($mode == 2)
		{
			//$this->contentTable = $this->getPageByTag($phrase, $register);
			$output = getContentSites($this->sql, $this->getPageByTag($phrase, $register));
// 			$output = $this->display->displayPage($this->contentTable, $this->results_on_page, $page);
// 			$this->navigationString = $this->display->createNavigation($phrase, $this->contentTable, $this->results_on_page, $page);
		}

		return $output;
	}	
	
	/**
	 * Funckcja ma za zadanie wyszukac wszystkie strony, które posiadają podaną etykiete, a nastepnie posortowac wyniki
	 * @param string $phrase etykieta, po ktorej maja byc wyszukane strony
	 * @param int $register zmienna okreslajaca, czy ma byc rejestrowany czas wykonywania polecenia
	 * @return array tablica zawierajaca posortowane adresy stron od najbardziej trafionego (najwięcej wystąpień etykiety) do najmniej
	 */
	 //PRZEROBIC BO BĘDZIE ZLE WYSWIETLAC DANE!
	private function getPageByTag($phrase, $register = 0)
	{
		$search_number = array('_0_', '_1_', '_2_', '_3_', '_4_', '_5_', '_6_', '_7_', '_8_');
		$replace_polish =  array('ą',   'ć',   'ę',   'ł',   'ń',   'ó',   'ś',   'ż',   'ź');		
		$phrase = @str_replace($search_number, $replace_polish, $phrase);
		$pages = $this->searchPageByTag($phrase, $register);
		for ($i = 0; $i < @count($pages); $i++)
		{
			for ($j = $i + 1; $j < @count($pages); $j++)
			{
				if ($pages[$i][1] < $pages[$j][1])
				{
					//ŻE CO??! tutaj zamienić na przenoszenie nadrzędnej tabeli, bez podtabel
// 					$tmp[0] = $pages[$i][0];
// 					$tmp[1] = $pages[$i][1];
					$tmp = $pages[$i];
// 					$pages[$i][0] = $pages[$j][0];
// 					$pages[$i][1] = $pages[$j][1];
					$pages[$i] = $pages[$j];
// 					$pages[$j][0] = $tmp[0];
// 					$pages[$j][1] = $tmp[1];
					$pages[$j] = $tmp;
				}
			}
			$return_array[$i] = $pages[$i][0];
		}
		return $return_array;
	}
	
	/**
	 * Funkcja przeszukuje baze po zadanym slowie-etykiecie i zwraca wszystkie adresy stron spelniajace kryteria
	 * @param string $phrase szukane slowo
	 * @param int $registry_time zmienna okreslajaca, czy ma byc rejestrowany czas wykonywania polecenia
	 * @return array zwraca tablice zawierajaca podtablice - element pierwszy to adres strony, drugi to priorytet etykiety (czestotliwosc wystapien)
	 */
	private function searchPageByTag($phrase, $registry_time)
	{
		$return_array;
		$start_time = $this->getmicrotime();
		$phrase = md5($phrase);
		$phrase = substr($phrase, 0, 5) . substr($phrase, -5);
		$this->sql->buildConditionQuery(array('tags_list', 'tag_hash'), array($phrase), DataEnum::EQUAL);
		$this->sql->queryTable(array('tags_list' => array('tag_id'/*, 'tag_word'*/)), $this->sql->getConditions(), 1);
		$this->sql->flushConditions();
		//usprawnic - przypisać wartości z bazy do return array i raz tylko wykonac opróżnienie!
		if ($this->sql->getResults(0) > 0) {
			$this->sql->buildConditionQuery(array('tags', 'tag_word'), $this->sql->getResults(0)['tag_id'], DataEnum::EQUAL);
			$this->sql->buildConditionQuery(array('tags', 'active'), array(1), DataEnum::EQUAL, DataEnum::DAND);
			$this->sql->queryTable(array('tags' => array('site')), $this->sql->getConditions(), -1);
			$this->sql->flushConditions();
			if($this->sql->getResults(1) !== -1) {
				$this->sql->flushResults();
				return false;
			}
			$return_array = $this->sql->getResults(1)['site'];
			$this->sql->flushResults();
		}
		else {
			$this->sql->flushResults();
			return false;
		}
// 		$this->query = "SELECT `page`, `amount` FROM `".ustawienia::$prefix."index_tags` WHERE `tag` = '".$phrase."' AND `priority` > '0';";
// 		$this->execute_info = @mysql_query($this->query);
// 		if (@mysql_num_rows($this->execute_info) == 0) return false;
// 		$i = 0;
// 		while ($result = mysql_fetch_row($this->execute_info))
// 		
// 		{
// 			$return_array[$i][0] = $result[0];
// 			$return_array[$i][1] = $result[1];
// 			$i++;
// 		}
		$end_time = $this->getmicrotime();
		if ($registry_time == 1)
		{
			$time = $end_time - $start_time;
			//tutaj dopisać kod rejestracji czasu zapytania - stary jest tragiczny...
/*			$this->query = "INSERT INTO  `".ustawienia::$prefix."index_log` VALUES ('', '".$phrase."', 'search_page_by_tag', CURDATE(), '".$time."');";
			$this->execute_info = @mysql_query($this->query);*/	
		}
		return $return_array;
	}
	
	/**
	 * Funkcja zwraca wszystkie etykiety przypisane do podanego adresu strony 
	 * @param string $address adres strony, dla ktorej maja byc wyszukane etykiety
	 * @param int $registry_time zmienna okreslajaca, czy ma byc rejestrowany czas wykonywania polecenia
	 * @return array tablica z etykietami oraz ich priorytetem (tablica zawiera podtablice)
	 */
	private function getTags($address = "", $registry_time = 0)
	{
		$return_array;
		$exist = false;
		$start_time = $this->getmicrotime();
		$this->sql->buildConditionQuery(array('tags', 'site'), array($address), DataEnum::EQUAL);
		$this->sql->queryTable(array('tags' => array('tag_word')), $this->sql->getConditions(), -1);
		$this->sql->flushConditions();
		
		if ($this->sql->getResults(0) > 0) {
			$this->sql->buildConditionQuery(array('tags_list', 'tag_id'), $this->sql->getResults(0)['tag_word'], DataEnum::EQUAL);
			$this->sql->queryTable(array('tags_list' => array('tag_word')), $this->sql->getConditions(), -1);
			$return_array = $this->sql->getResults(1)['tag_word'];			
		}
		$this->sql->flushResults();
		if (!is_array($return_array)) return false;
// 		$address_parts = @explode('/', $address);	
// 		if (@count($address_parts) == 7) $this->query = "SELECT `tag`, `priority` FROM `".ustawienia::$prefix."index_tags` WHERE `page` like '%".$address_parts[5]."%' AND `priority` > '0';";	
// 		else if (@strcmp($address_parts[1], "events") == 0 ) $this->query = "SELECT `tag`, `priority` FROM `".ustawienia::$prefix."index_tags` WHERE `page` LIKE '%events%' AND `priority` > '0';";
// 		else if (@strcmp($address_parts[1], "projects") == 0) $this->query = "SELECT `tag`, `priority` FROM `".ustawienia::$prefix."index_tags` WHERE `page` LIKE '%".$address_parts[2]."%' AND `priority` > '0';";
// 		else if (@strcmp($address_parts[1], "news") == 0) $this->query = "SELECT `tag`, `priority` FROM `".ustawienia::$prefix."index_tags` WHERE `page` LIKE '%news%' AND `priority` > '0';";		
// 		else $this->query = "SELECT `tag`, `priority` FROM `".ustawienia::$prefix."index_tags` WHERE `priority` > '0' LIMIT 30;";	
// 		$this->execute_info = @mysql_query($this->query);
// 		if (@mysql_num_rows($this->execute_info) == 0) return false;
// 		$i = 0;
// 		while ($result = mysql_fetch_row($this->execute_info))
// 		{
// 			$exist = false;
// 			for ($j = 0; $j < @count($return_array); $j++)
// 			{
// 				if (@strcmp($return_array[$j][0], $result[0]) == 0)
// 				{
// 					$return_array[$j][1]+=$result[1];
// 					$exist = true;
// 					break;
// 				}
// 			}
// 			if (!$exist)
// 			{				
// 				$return_array[$i][0] = $result[0];
// 				$return_array[$i][1] = $result[1];
// 				$i++;
// 			}			
// 		}
		$end_time = $this->getmicrotime();
		if ($registry_time == 1)
		{
			//trzeba dopracować wyświetlanie czasu operacji - o ile będzie potrzebne
			$time = $end_time - $start_time;
/*			$this->query = "INSERT INTO `".ustawienia::$prefix."index_log` VALUES ('', '".$address."', 'search_tag', CURDATE(), '".$time."');";
			$this->execute_info = @mysql_query($this->query);*/	
		}
		return $return_array;
	}
	
	/**
	 * Funkcja dodaje nowe etykiety do tabeli tags/tags_list 
	 * @param string $tags_list etykiety, ktore dodawane sa automatycznie z tekstu strony
	 * @param string $address adres strony, dla ktorej dodawane sa etykiety
	 */
	 //POPRAWIĆ funkcje, w których wywoływana jest ta poniżej
	 //smaą funkcję dokończyć!!
	private function insertTagsToTable($tags_list, $address)
	{
		//$this->sql->buildConditionQuery(array('tags', 'site'), array($address), DataEnum::EQUAL);
		//$this->sql->queryTable(array('tags' => array('tag_word')), $this->sql->getConditions(), -1);
		
		//$this->sql->flushConditions();
// 		$exist = false;
		if (@strcmp($tags_list, "") != 0)
		{
			$tags_list = $tags_list.'||';
			$this->sql->callProcedure('addtagslist', array($tags_list, $address)); 
// 			$this->query = "SELECT add_tags('$tags_list', '$address', 'orange_');";
// 			$this->execute_info = @mysql_query($this->query);
		}
	}
	
	/**
	 * Funkcja ma za zadanie przeszukac indeks pod wzgledem wystepowania podanej frazy i zwrocic strony, ktore do niej pasuja
	 * @param string $phrase lista slow do przeszukania indeksu
	 * @param unknown_type $register zmienna okreslajaca, czy ma byc rejestrowany czas wykonywania polecenia
	 * @return array lista stron, ktore zostaly znalezione
	 */
	private function searchPhrase($phrase, $register)
	{
		$phrase = $this->changePhrase($phrase);
		$phrases_list = $this->searchDatabaseIndex($phrase, $register);
		$array_list = $this->priorityPhrase($phrases_list);
// 		if (@count($array_list) == 0) $array_list[0] = "Nic nie znaleziono";
		if (@count($array_list) == 0) $array_list[0] = "";
	    return $array_list;
	}	
	
	/**
	 * Funkcja ma za zadanie skasowac z indeksu odwolanie do podanej strony
	 * @param string $address adres strony, ktora ma zostac usunieta z indeksu
	 * @return bool zwraca TRUE w przypadku powodzenia lub FALSE, jezeli nie mozna bylo skasowac strony (np. już nie istniala)
	 */
	private function deletePageFromIndex($address)
	{		
		$this->sql->buildConditionQuery(array('words_sites', 'site'), array($address), DataEnum::EQUAL);
		$this->sql->delete('words_sites', $this->sql->getConditions());
		$this->sql->flushConditions();
		return true;		
	}
	
	/**
	 * Funkcja kasuje z etykiet podana strone
	 * @param string $address adres strony do skasowania
	 * @return bool zwraca TRUE w przypadku skasowania strony
	 */
	private function deletePageFromTags($address)
	{
		$this->sql->buildConditionQuery(array('tags', 'site'), array($address), DataEnum::EQUAL);
		$this->sql->delete('tags', $this->sql->getConditions());
		$this->sql->flushConditions();
		return true;
	}
	
	/**
	 * Funkcja ma za zadanie skasowac wybrana etykiete dla podanej strony z bazy; w rzeczywistosci ona nie kasuje a usuwa wyswietlanie sie danej etykiety
	 * @param string $phrase etykieta, ktora ma zostac skasowana
	 * @param string $address adres strony, dla ktorej ma zostac skasowana etykieta
	 * @return bool zwraca TREU w przypadku skasowania etykiety
	 */
	private function deleteTagFromTags($phrase, $address)
	{
		$hash = $this->buildHash($phrase);
// 		$hash = md5($phrase);
// 		$hash = substr($hash, 0, 5) . substr($hash, -5);
		$this->sql->buildConditionQuery(array('tags_list', 'tag_hash'), array($hash[6]), DataEnum::EQUAL);
		$this->sql->queryTable(array('tags_list' => array('tag_id')), $this->sql->getConditions(), -1);
		$this->sql->flushConditions();
		$this->sql->buildConditionQuery(array('tags', 'tag_word'), $this->sql->getResults(0)['tag_id'], DataEnum::EQUAL);
		$this->sql->update('tags', array('active' => '0'), $this->sql->getConditions());
		$this->sql->flushConditions();
		$this->sql->flushResults();
// 		$this->query = "UPDATE `".ustawienia::$prefix."index_tags` SET `priority` = '0' WHERE `tag` = '".$phrase."' AND `page` = '".$address."';";
// 		$this->execute_info = @mysql_query($this->query);
		return true;
	}
	
	/**
	 * Funkcja kasuje wybrane slowo z bazy indeksu 
	 * @param string $phrase slowo do skasowania
	 * "return bool funkcja zwraca wartosc TRUE w przypadku powodzenia lub FALSE w przypadku niepowodzenia (np. nie znaleziono podanej frazy w indeksie)
	 */
	private function deleteFromIndex($phrase)
	{
		//slowa raczej nie beda kasowane ze slownika (po co?)
		return false;
	}
	
	/**
	 * Funkcja filtruje slowa pod wzgledem dlugosci jak i wystepowania w nich zabronionych tresci (wartosci liczbowych)
	 * @param string $phrase slowa do przefiltrowania
	 * @param int $mode tryb pracy; 0 - akcepruje slowa conajmniej 5 literowe; 1 - akceptuje slowa od dwoch liter 
	 * @return array zwraca przefiltrowana tablice slow, gotowych do wstawienia do bazy indeksu lub etykiet
	 */
	private function filtrKeyword($phrase, $mode = 1)
	{
		$minimum_string_length = 1;
		if ($mode == 0) $minimum_string_length = 4;
		$return_array = array();
		$phrases_list = explode(' ', $phrase);		
		for ($i = 0; $i < @count($phrases_list); $i++)
		{ 	
			if ((@strlen($phrases_list[$i]) > $minimum_string_length) && !(@preg_match('/[0-9]+/',$phrases_list[$i]))) 
			{		 
				if (($index = @array_search($phrases_list[$j], $return_array)) === false) $return_array[@count($return_array)] = $phrases_list[$i];				
			}
		}
		return $return_array;
	}	
	
	/**
	 * Funkcja na poczatku grupuje strony z kazdego wyszukiwania do jednej tablicy, zapisujac ilosc wystapienia; nastepnie wedlug tego numeru ustawia je na liscie do wyswietlenia
	 * @param array $phrases_list tablica zawierajca tablice adresow z wyszukiwania kazdego ze slow
	 * @return array zwraca liste adresow stron ustwionych od najwyzszej zawartosci wyszkiwanej frazy do najnizszej
	 */
	private function priorityPhrase($phrases_list)
	{
		if (!isset($phrases_list[0][1])) return;
		$return_array;
		$tmp_site;
		for ($i = 0; $i < count($phrases_list); $i++) {
			if (is_array($phrases_list[$i][1]) || $phrases_list[$i][1] !== '') {
				//sprawdzić poprawność rozumowania tego podejścia
				//jeżeli zgadzają si≥ę wszystkie hash <- dokładnie tego słowa szukamy
				//zgadza sie 5 pierwszych <- większość słowa się zgadza
				//itd. aż słowo jest zgodne tylko z jednym hashem (2 bądź 6) i w związku z powyższym nadajemy najniższy priorytet
				//POPRAWIĆ!!!!!!!!!!!!!!!!!!
// 				if ($phrases_list[$i][0]['hash6'] && $phrases_list[$i][0]['hash5'] && 
// 				$phrases_list[$i][0]['hash4'] && $phrases_list[$i][0]['hash3'] && $phrases_list[$i][0]['hash2'])
// 					$tmp = 5;
// 				else if ($phrases_list[$i][0]['hash5'] && $phrases_list[$i][0]['hash4'] && 
// 				$phrases_list[$i][0]['hash3'] && $phrases_list[$i][0]['hash2'])
// 					$tmp = 4;
// 				else if ($phrases_list[$i][0]['hash4'] && 
// 				$phrases_list[$i][0]['hash3'] && $phrases_list[$i][0]['hash2'])
// 					$tmp = 3;
// 				else if ($phrases_list[$i][0]['hash3'] && $phrases_list[$i][0]['hash2'])
// 					$tmp = 2;
// 				else
					$tmp = 1;
				for ($j = 0; $j < count($phrases_list[$i][1]); $j++) {
					if (isset($tmp_site)) {
						$keys = array_keys($tmp_site);
						$pos = array_search($phrases_list[$i][1][$j], $keys);
					}	
					else
						$pos = false;
						
					if ($pos)
						$tmp_site[$keys[$pos]] += 1 * $tmp;
					else
						$tmp_site[$phrases_list[$i][1][$j]] = 1 * $tmp;
				}
			}
		}
		//możliwe, że trzeba będzie odwrócić wartość klucz <=> zapisana wartość
		if (!isset($tmp_site)) return;
		if (!is_array($tmp_site)) return;
		arsort($tmp_site);		
		$keys = array_keys($tmp_site);
		for($i = 0; $i < count($keys); $i++)
			$return_array[ @count($return_array) ] = $keys[$i];
		return $return_array;
	}
	
	/**
	 * Funkcja pobiera czas w liczony mikrosekundach 
	 */
	private function getmicrotime()
	{	
		list($usec, $sec) = explode(" ",microtime());
		return ((float)$usec + (float)$sec);
	} 
	
	/**
	 * Funkcja przeszukuje indeks dla podanych slow; ponadto funkcja jest w stanie rejestrowac czas wykonania zapytania
	 * @param string $phrase szukane slowa, oddzielone spacja
	 * @param int $registry_time zmienna okreslajaca, czy ma byc rejestrowany czas wykonywania polecenia
	 * @return array zwraca dwuwymiarowa tablice wynikow 
	 */
	private function searchDatabaseIndex($phrase = "", $registry_time = 0)
	{		
		$return_array;
		//$array_index = 0;
		$start_time = $this->getmicrotime();
		$phrases_list = @explode(' ', $phrase);		
		for ($i = 0; $i < @count($phrases_list); $i++)
		{	
			//skład $r:
			// [0] - zawiera hash szukanego słowa (późniejsza priorytetyzacja wyniku)
			// [1] - zawiera listę stron, do których słowo pasuje
			$r = $this->mainSelectSql($phrases_list[$i]);			
			if ($r !== false)
				$return_array[ @count($return_array) ] = $r;
		}
		$end_time = $this->getmicrotime();
		if ($registry_time == 1)
		{
			//może kiedyś....
			$time = $end_time - $start_time;
// 			$this->query = "INSERT INTO `".ustawienia::$prefix."index_log` VALUES ('', '".$phrase."', 'search_index', CURDATE(), '".$time."')";
// 			$this->execute_info = @mysql_query($this->query);	
		}
		if (!isset($return_array)) return;
		return $return_array;
	}
	
	/**
	 * Funkcja dodaje do bazy indeksow slowa kluczowe
	 * @param string $phrases_list lista slow, ktore beda dodane do bazy; ciag musi zaczynac sie i konczyc znakami "||"; poszczegolne slowa rowniez musza byc nimi oddzielone 
	 * @param string $address adres strony, do ktorej beda odnosic poszczegolne slowa
	 */
	private function insertToIndex($phrases_list, $address)
	{
		$this->sql->callProcedure('addIndexList', array($phrases_list, $address));
	}
	
	/**
	 * Funkcja przeszukuje podindeks pod katem wystepowania podanego slowa
	 * @param string $search_keyword slowo, ktore chcemy wyszukac
	 * @return mixed zwraca liste stron (string), do ktorych odnosi sie slowo lub FALSE jezeli nie ma slowa
	 */
	private function subSelectSql($search_keyword = "")
	{
		//prawdopodobnie w całości do wyrzucenia!!!!!
		//brak podzapytań - jest tylko jeden indeks (główny)!
// 		$this->query = "SELECT `doc_list` FROM `".ustawienia::$prefix."index_sub` WHERE `tag` = '".$search_keyword."';";
// 		$this->execute_info = @mysql_query($this->query);
// 		if (mysql_num_rows($this->execute_info) == 0) return false;
// 		$result = mysql_fetch_row($this->execute_info);	
// 		return  $result[0];
	}
	
	private function buildHash($r) {
		$ret;
		for ($i = 6; $i > 1; $i--) {
			$r = substr($r, 0, $i);
			$hash = md5($r);
			$ret[$i] = substr($hash, 0, 10);
		}
		return $ret;
	}
	
	/**
	 * Funkcja przeszukuje glowny indeks pod katem wystepowania podanego slowa
	 * @param string $search_keyword slowo, ktore chcemy wyszukac
	 * @return bool zwraca TRUE w rzpypadku odnalezienia lub FALSE w przypadku niewystapienia slowa
	 */
	private function mainSelectSql($search_keyword = "")
	{
		$return_array;
		$hash = $this->buildHash($search_keyword);
// 		$search_keyword = substr($search_keyword, 0, 6);
// 		$hash = md5($search_keyword);
// 		$hash = substr($hash, 0, 10);// . substr($hash, -5);
		
		$this->sql->buildConditionQuery(array('words', 'hash2'), array($hash[2]), DataEnum::EQUAL);
		$this->sql->buildConditionQuery(array('words', 'hash3'), array($hash[3]), DataEnum::EQUAL, DataEnum::DOR);
		$this->sql->buildConditionQuery(array('words', 'hash4'), array($hash[4]), DataEnum::EQUAL, DataEnum::DOR);
		$this->sql->buildConditionQuery(array('words', 'hash5'), array($hash[5]), DataEnum::EQUAL, DataEnum::DOR);
		$this->sql->buildConditionQuery(array('words', 'hash6'), array($hash[6]), DataEnum::EQUAL, DataEnum::DOR);
		$this->sql->queryTable(array('words' => array('word_id', 'hash2', 'hash3', 'hash4', 'hash5', 'hash6')), $this->sql->getConditions(), 1);
		$this->sql->flushConditions();
		if ($this->sql->getResults(0) !== -1) {
			$keys = array_keys($this->sql->getResults(0));
			for ($i = 0; $i < count($keys); $i++)
				$return_array[0][$keys[$i]] = $this->sql->getResults(0)[$keys[$i]][0];
			$this->sql->flushResults();
			$this->sql->buildConditionQuery(array('words_sites', 'word_id'), array($return_array[0]['word_id']), DataEnum::EQUAL);
			$this->sql->buildConditionQuery(array('words_sites', 'active'), array(1), DataEnum::EQUAL, DataEnum::DAND);
			$this->sql->queryTable(array('words_sites' => array('site')), $this->sql->getConditions(), -1);
			$this->sql->flushConditions();
			$return_array[1] = $this->sql->getResults(0)['site'];		
		}
		$this->sql->flushResults();
		if (!isset($return_array)) return false;
		if (!is_array($return_array))
			return false;
		else
			return $return_array;
			
	}
	
	/**
	 * Funkcja zamienia wszystkie litery w ciagu na male, po czym filtruje tresc z nieporzadanych znakow
	 * @param string $phrase przeszukiwany ciag
	 * @param int $mode tryb pracy; 0 - zamienia polskie znaki odpowiedniki (np. 'ó' -> 'o'); 1 - poslkie znaki sa zachowywane
	 * @return string zwraca zamieniony ciag
	 */
	private function changePhrase($phrase, $mode = 0)
	{			
		$phrase = @mb_strtolower($phrase,"UTF-8");	
		$return_string = "";
		$return_string = $this->searchForUnsupportedChar($phrase, $mode);		
		return $return_string;
	}
	
	/**
	 * Funkcja przeszukuje ciag i zamienia badz usuwa znaki, ktore nie sa obslugiwane
	 * @param string $string przeszukiwany ciag
	 * @param int $mode tryb pracy; 0 - zamienia polskie znaki odpowiedniki (np. 'ó' -> 'o'); 1 - poslkie znaki sa zachowywane
	 * @return zwraca zmieniony ciag
	 */
	private function searchForUnsupportedChar($string, $mode)
	{
		$start = $this->getmicrotime();
		$search_polish_html =  array("&oacute;");
		$replace_polish_html = array('ó');
		$search_polish =  array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', 'ź', "&oacute;");
		$replace_polish = array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'o');
		$search_special = array("&lsquo;", "&bdquo;", "&rdquo;", "&ldquo;", "&sbquo;", "&rsquo;", "&ndash;", "&nbsp;", ',', '.', '/', '<', '>', '?', ';', '\'', ':', 
		'"', '[', ']', '{', '}', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '|', '-', '=', '\\',);
		$replace_special = array('');
		$search_newline = array("\r\n");
		$replace_newline = array(' ');		
		if ($mode == 0) $string = @str_replace($search_polish, $replace_polish, $string);
		if ($mode == 1) $string = @str_replace($search_polish_html, $replace_polish_html, $string);
		$string = @str_replace($search_special, $replace_special, $string);	
		$string = @str_replace($search_newline, $replace_newline, $string);		
		return $string;
	}
}
?>
