<figure>
	<div id="imgSplash" style="">
		<img src="img/pte.png" alt="Logo PTE"/>
	</div>
	<figcaption>	
		<h3>Tytuł opisu</h3>
		<p>Tutaj może być dowolny tekst bądź można to pole skasować</p>
	</figcaption>
</figure>
<section id="news">
	<h2>Aktualności</h2>
	<article>
		<?php echo $news; ?>
	</article>
</section>

<section id="congress">
	<h2>kongres</h2>
	<nav id="congressNav">
		<ul>
			<li data-name="conSubject"><p>Tematyka</p></li>
			<li data-name="conOrganizators"><p>Organizatorzy</p></li>
			<li data-name="conCommittee"><p>Komitet naukowy</p></li>
			<li data-name="conPresentation"><p>Forma prezentacji</p></li>
			<li data-name="conTerms"><p>Ważne terminy</p></li>
			<li data-name="conFees"><p>Opłaty i zgłoszenia</p></li>
			<li data-name="conPartnerShip"><p>Partnerzy</p></li>
		</ul>
	</nav>
	<article>
		<div id="conSubject">
			<h3><?php echo $content['conSubject']['header']; ?></h3>
			<?php echo $content['conSubject']['text']; ?>
		</div>
	</article>
	<article>
		<div id="conOrganizators">
			<h3><?php echo $content['conOrganizators']['header']; ?></h3>
			<?php echo $content['conOrganizators']['text']; ?>
		</div>
	</article>
	<article>
		<div id="conCommittee">
			<h3><?php echo $content['conCommittee']['header']; ?></h3>
			<?php echo $content['conCommittee']['text']; ?>                                               
		</div>
	</article>
	<article>
		<div id="conPresentation">
			<h3><?php echo $content['conPresentation']['header']; ?></h3>
			<?php echo $content['conPresentation']['text']; ?> 
		</div>
	</article>
	<article>
		<div id="conTerms">
			<h3><?php echo $content['conTerms']['header']; ?></h3>
			<?php echo $content['conTerms']['text']; ?> 
		</div>
	</article>
	<article>
		<div id="conFees">
			<h3><?php echo $content['conFees']['header']; ?></h3>
			<?php echo $content['conFees']['text']; ?>
		</div>
	</article>
	<article>
		<div id="conPartnerShip">
			<h3><?php echo $content['conPartnerShip']['header']; ?></h3>
			<?php echo $content['conPartnerShip']['text']; ?>
		</div>
	</article>
</section>
<section class="iconsFont" id="about">
	<h2><?php echo $content['about']['header']; ?></h2>
	<article>
		<div class="iconSection"><i class="fa fa-info-circle" aria-hidden="true"></i></div>
		<div><?php echo $content['about']['text']; ?></div>
	</article>
</section>
<section class="iconsFont" id="auth">
	<h2><?php echo $content['auth']['header']; ?></h2>
	<article>
		<div class="iconSection"><i class="fa fa-id-card-o" aria-hidden="true"></i></div>
		<div><?php echo $content['auth']['text']; ?></div>
	</article>
</section>
<section id="partnership">
	<h2>Współpraca</h2>
</section>
<section id="contact" title="By Pudelek (Marcin Szala) (Own work(own work by uploader)) [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY-SA 4.0-3.0-2.5-2.0-1.0 (http://creativecommons.org/licenses/by-sa/4.0-3.0-2.5-2.0-1.0)], via Wikimedia Commons">
	<h2>Kontakt</h2>
	<h4 class="contactLangTip"><span></span><div id="plFlag" data-lang="pl-PL" title="język polski"></div><div id="enFlag" data-lang="en-UK" title="english language"></div></h4>
	<article>
		<div id="addressDiv" title="">
		<p>"DOM EKONOMISTY"
		<p>ul. Kilińskiego 32/34
		<p>42-200 Częstochowa

		<p>tel. 34 / 324 97 33, 324 26 30
		<p>e-mail: opteczwa@onet.pl
		<p>www.czestochowa.pte.pl
		<p>NIP: 573-10-87-87
		<p>PKO Bank Polski: 73 1020 1664 0000 3202 0166 1818
		</div>
		<div id="formContactDiv" title="">
			<div id="uploadGauge"><div><p>Postęp przesyłania </p><p style="margin-left: 10px;"></p></div></div>
			<div><button onclick="sendMailJS();">Wyślij</button></div>
			<input type="hidden" name="sid" value="<?php echo session_id();?>"/>
		</div>
	</article>
</section>
<section id="map">
	<h2>Dojazd</h2>
	<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDsApz-B-zMXEISpcVML4GfV8F1jd9_b7k&amp;q=Jana+Kilińskiego+32%2F34,+Częstochowa&amp;language=pl&amp;center=50.8156102,19.114128&amp;zoom=18" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen="">Mapa nie mogła zostać wyświetlona - brak dostępu do sieci internet bądź błąd usługi. Spróbuj przeładować stronę.</iframe>
</section>