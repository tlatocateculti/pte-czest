<?php 
	require_once "../scripts/utilityFunctions.php";
	if (!isset($_GET)) return;
	if ($_GET['s'] === "splashImages")
		echo file_get_contents('../json/gallery.json');
	else if ($_GET['s'] === "welcomeText")
		echo file_get_contents('../json/splashText.json');
	else if ($_GET['s'] === "contactFields")
		echo file_get_contents('../json/form.json');