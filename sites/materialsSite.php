<figure>
	<div id="imgSplash" style="">
		<img src="img/pte.png" alt="Logo PTE"/>
	</div>
	<figcaption>	
		<h3>Tytuł opisu</h3>
		<p>Tutaj może być dowolny tekst bądź można to pole skasować</p>
	</figcaption>
</figure>
<section id="news">
	<h2>Aktualności</h2>
	<article>
		<?php echo $news; ?>
	</article>
</section>

<section id="congress">
	<h2>kongres</h2>
	<nav id="congressNav">
		<ul>
			<li data-name="conSubject"><p>Tematyka</p></li>
			<li data-name="conOrganizators"><p>Organizatorzy</p></li>
			<li data-name="conCommittee"><p>Komitet naukowy</p></li>
			<li data-name="conPresentation"><p>Forma prezentacji</p></li>
			<li data-name="conTerms"><p>Ważne terminy</p></li>
			<li data-name="conFees"><p>Opłaty i zgłoszenia</p></li>
			<li data-name="conPartnerShip"><p>Partnerzy</p></li>
		</ul>
	</nav>
	<article>
		<div id="conSubject">
			<h3><?php echo $content['conSubject']['header']; ?></h3>
			<?php echo $content['conSubject']['text']; ?>
		</div>
	</article>
	<article>
		<div id="conOrganizators">
			<h3><?php echo $content['conOrganizators']['header']; ?></h3>
			<?php echo $content['conOrganizators']['text']; ?>
		</div>
	</article>
	<article>
		<div id="conCommittee">
			<h3><?php echo $content['conCommittee']['header']; ?></h3>
			<?php echo $content['conCommittee']['text']; ?>                                               
		</div>
	</article>
	<article>
		<div id="conPresentation">
			<h3><?php echo $content['conPresentation']['header']; ?></h3>
			<?php echo $content['conPresentation']['text']; ?> 
		</div>
	</article>
	<article>
		<div id="conTerms">
			<h3><?php echo $content['conTerms']['header']; ?></h3>
			<?php echo $content['conTerms']['text']; ?> 
		</div>
	</article>
	<article>
		<div id="conFees">
			<h3><?php echo $content['conFees']['header']; ?></h3>
			<?php echo $content['conFees']['text']; ?>
		</div>
	</article>
	<article>
		<div id="conPartnerShip">
			<h3><?php echo $content['conPartnerShip']['header']; ?></h3>
			<?php echo $content['conPartnerShip']['text']; ?>
		</div>
	</article>
</section>
<section class="iconsFont" id="about">
	<h2><?php echo $content['about']['header']; ?></h2>
	<article>
		<div class="iconSection"><i class="fa fa-info-circle" aria-hidden="true"></i></div>
		<div><?php echo $content['about']['text']; ?></div>
	</article>
</section>
<section class="iconsFont" id="auth">
	<h2><?php echo $content['auth']['header']; ?></h2>
	<article>
		<div class="iconSection"><i class="fa fa-id-card-o" aria-hidden="true"></i></div>
		<div><?php echo $content['auth']['text']; ?></div>
	</article>
</section>