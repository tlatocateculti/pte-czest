<?php 
	require_once "./scripts/utilityFunctions.php";
	$content = readEntries('./json', 'news.json');
	$news = '';
	$limit = ($site === 'archive') ? 0 : ((count($content) - 3 >= 0) ? count($content) - 3 : 0);
	for ($i = count($content) - 1; $i >= $limit; $i--) {
		$info = $content[$i];
		if (!$info['active']) continue;
		//((isset($info['gallery'])) ? "data-gallery='" . json_encode($info['gallery']) . "'" : "")
		$news .= "<div class=\"newsObj\" style=\"" . ((isset($info['gallery'])) ? "background-image: url('{$info['gallery'][0]['mini']}');" : "") . "\">
				<div class=\"newsText\" " . ((isset($info['gallery'])) ? "data-gallery='" . json_encode($info['gallery']) . "'" : "") . ">
					<h3>{$info['newsHeader']}</h3>
					<p>{$info['shortText']}</p>
					<div class=\"newsMore\"><p>więcej...</p></div>
					<div style=\"display: none;\">" . rawurldecode(urldecode(base64_decode($info['text']))) . "</div>
				</div>
			</div>";
			//wczesniej w naglowku byla data
// 			<h3>{$info['newsHeader']}<span style=\"font-style: italic; font-size: 12px\">" . explode(' ', $info['addDate'])[0] . "</span></h3>
	}