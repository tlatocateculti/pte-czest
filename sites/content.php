<?php 
	require_once "./scripts/utilityFunctions.php";
	$files = getJSONFiles('./json/sites');
	if (count($files) === 0) {
		$content = readEntries('./json', 'sites.json');
		$content['about']['text'] = rawurldecode(urldecode(base64_decode($content['about']['text'])));
		$content['auth']['text'] = rawurldecode(urldecode(base64_decode($content['auth']['text'])));
		$content['conSubject']['text'] = rawurldecode(urldecode(base64_decode($content['conSubject']['text'])));
		$content['conOrganizators']['text'] = rawurldecode(urldecode(base64_decode($content['conOrganizators']['text'])));
		$content['conCommittee']['text'] = rawurldecode(urldecode(base64_decode($content['conCommittee']['text'])));
		$content['conPresentation']['text'] = rawurldecode(urldecode(base64_decode($content['conPresentation']['text'])));
		$content['conTerms']['text'] = rawurldecode(urldecode(base64_decode($content['conTerms']['text'])));
		$content['conFees']['text'] = rawurldecode(urldecode(base64_decode($content['conFees']['text'])));
		$content['conPartnerShip']['text'] = rawurldecode(urldecode(base64_decode($content['conPartnerShip']['text'])));
	}
	else {
		foreach($files as $file) {
			$readJSON = readEntries('./json/sites', $file);
			$cName = array_keys($readJSON)[0];
			$readFile = (!isset($readJSON[$cName]['siteCategory'])) ? true : (($readJSON[$cName]['siteCategory'] === $site) ? true : false);  
			if ($readFile) {
				$content[$cName] = $readJSON[$cName];
				$content[$cName]['text'] = rawurldecode(urldecode(base64_decode($content[$cName]['text'])));
				$content[$cName]['sectionID'] = $cName;
			}
		}
	}